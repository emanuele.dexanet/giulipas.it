<?php

/**
 * Loading developers intruments
 */

require get_template_directory().'/lib/inc/plugins-activation/plugins-activation.php';

/**
 * MINIFY MENU SWITCHER
 *
 * Note that the Minify menu appears
 * only for the first username like
 * the s devMenu
 */   
 
if (isset($_GET['minify'])) {               

    switch ($_GET['minify']) {
      case 'off':
      update_option( 'minify', 'OFF' );
      if ($user_id == 1) {
      add_action('admin_bar_menu', 'create_minify_menu', 2000);
      } 
      break;  

      case 'on':
      update_option( 'minify', 'ON' );
      if ($user_id == 1) {
      add_action('admin_bar_menu', 'create_minify_menu', 2000);
      }
      break;  

      default:
      if ($user_id == 1) {
      add_action('admin_bar_menu', 'create_minify_menu', 2000);
      }
      break;
}} else {
  if ($user_id == 1) {
    add_action('admin_bar_menu', 'create_minify_menu', 2000);
  } 
}

function create_minify_menu() {
    global $wp_admin_bar;
    $myOptions = get_option('minify');

    switch ($myOptions) {
    case 'ON':
    $minicon = '<img src="'.get_template_directory_uri().'/lib/inc/img/minify.png" style="position: absolute;top: 10px;right: 2px;">';
    $mintitle = 'Minify &nbsp;&nbsp;&nbsp; '.$minicon;
    $minhref = get_admin_url().'?minify=off';
    // Passo le variabili definite in config.php al compilatore esterno di css e js
    $mUrl = get_template_directory_uri().'/lib/inc/skin/minify.php';
    $handle = curl_init($mUrl);
    curl_setopt($handle, CURLOPT_POST, true);
    curl_setopt($handle, CURLOPT_POSTFIELDS, $GLOBALS['assets_options']);
    curl_exec($handle);
    break;
  
    default:
    $minicon = '<img src="'.get_template_directory_uri().'/lib/inc/img/minify_off.png" style="position: absolute;top: 10px;right: 2px;">';
    $mintitle = 'Minify &nbsp;&nbsp;&nbsp; '.$minicon;
    $minhref = get_admin_url().'?minify=on';
    break;
}

  $minmenu_id = 'minify-menu';
  $wp_admin_bar->add_menu(array(
    'id' => $minmenu_id, 
    'title' => __($mintitle),
    'href' => $minhref));   
 } // end minify menu