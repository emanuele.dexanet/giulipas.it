<?php

/**
 * MadFramework Custom Dashboard
 *
 * Questo modulo permette ai clienti di accedere al proprio pannello di controllo
 * o di inviare delle richieste urgenti direttamente su mobile.
 *
 */

add_action( 'wp_dashboard_setup', 'register_my_dashboard_widget' );
function register_my_dashboard_widget() {
	wp_add_dashboard_widget(
		'my_dashboard_widget',
		'Assistenza clienti',
		'my_dashboard_widget_display'
	);

}

function my_dashboard_widget_display() {
    // $dashboard = file_get_contents('http://127.0.0.1/dashboard_message.php?pass=dexa2014');
    $dashboard = 'Inserire messaggio Dashboard...';
    echo $dashboard;
}
