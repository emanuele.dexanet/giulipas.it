<?php
/**
 * Configuration values
 */
$ID_ga = get_option('ga_seo');
$ID_tgm = get_option('ga_tgm');
$ID_hjar = get_option('seo_hjar');
$ID_api_maps = get_option('googlemaps_key');
$aos_js = get_option('aos_js');
$font_awesome = get_option('font_awesome');
$animate_css = get_option('animate_css');
$hover_css = get_option('hover_css');
//Varie
add_theme_support('jquery-cdn');  // Enable to load jQuery from the Google CDN

//Google Analytics et similia
define('GOOGLE_TAG_MANAGER_ID', $ID_tgm); // Inserisci il codice del tag manager - se lo userai
define('GOOGLE_ANALYTICS_ID', $ID_ga); // UA-XXXXX-Y (Note: Universal Analytics only, not Classic Analytics)
define('HOTJAR_ANALYTICS_ID', $ID_hjar); // Inserisci il codice hjid di Hotjar

//Google maps
define('MAP_API_KEY', $ID_api_maps);

//// !!! ATTENZIONE LE LIBRERIE QUI SOTTO SONO ATTIVABILI DAL BACKEND DI WORDPRESS !!! ////
$GLOBALS['assets_options'] = array(
    'AOS'          => $aos_js,    // https://michalsnik.github.io/aos/
    'FONTAWESOME'  => $font_awesome,    // http://fontawesome.io/icons/
    'ANIMATECSS'   => $animate_css,    // https://daneden.github.io/animate.css/
    'HOVERCSS'     => $hover_css,    // http://ianlunn.github.io/Hover/
);
//// !!! ATTENZIONE LE LIBRERIE QUI SOPRA SONO ATTIVABILI DAL BACKEND DI WORDPRESS !!! ////
$GLOBALS['CSS'] = array(
    'Style'=> get_template_directory_uri().'/assets/includes/css/style.min.css',
    // Qui puoi aggiungere tutti i css che vuoi
);
// Carica i js nel header del sito
$GLOBALS['JS-HEADER'] = array(
    //'Jquery-min' => 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js',
    //'Boostrap-min' => 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
    //'Boostrap-th' => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js',
    // 'Gmaps' => 'https://maps.googleapis.com/maps/api/js?v=3&key='.MAP_API_KEY,
    // 'Maps' => get_template_directory_uri().'/assets/includes/js/map.js'
    // Qui puoi aggiungere tutti i js che vuoi
);
// Carica i js nel footer del sito
$GLOBALS['JS-FOOTER'] = array(
    // Qui puoi aggiungere tutti i js che vuoi
	'Popper'=> get_template_directory_uri().'/assets/includes/js/popper.min.js',
	'Boostrap-min'=> get_template_directory_uri().'/assets/includes/js/bootstrap.min.js',
);