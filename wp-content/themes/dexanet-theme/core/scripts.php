<?php
// Imposto gli script che mi interessano come async & defer
// in particolare google mappe.
function make_script_async( $tag, $handle, $src )
{
  if ( 'googlemaps' != $handle ) {
    return $tag;
  }

  return str_replace( '<script', '<script async defer', $tag );
}
add_filter( 'script_loader_tag', 'make_script_async', 10, 3 );


/**
 * CARICO I CSS E I JS
 */
function mad_scripts() {

  // Carico la lista di CSS
  foreach ($GLOBALS['CSS'] as $nome => $percorso) {
    wp_enqueue_style(  $nome, $percorso );
  }

  // Carico il CSS custom
  wp_enqueue_style(  'default', get_template_directory_uri() . '/assets/custom.css' );

  // jQuery is loaded using the same method from HTML5 Boilerplate:
  // Grab Google CDN's latest jQuery with a protocol relative URL; fallback to CDNJS if offline
  // It's kept in the header instead of footer to avoid conflicts with plugins.
  if (!is_admin() && current_theme_supports('jquery-cdn')) {
    wp_deregister_script('jquery');
	wp_register_script('jquery', get_template_directory_uri().'/assets/includes/js/jquery.min.js', array(), null, true);
    /*add_filter('script_loader_src', 'mad_jquery_local_fallback', 10, 2);*/
  }

  wp_enqueue_script('jquery');

  // Gestisco i JS per i commenti
  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  // Carico la lista di JS header
  foreach ($GLOBALS['JS-HEADER'] as $nome => $percorso) {
    wp_enqueue_script(  $nome, $percorso );
  }
  // Carico la lista di JS footer
  foreach ($GLOBALS['JS-FOOTER'] as $nome => $percorso) {
    wp_enqueue_script(  $nome, $percorso,'','',true );
  }
  // Carico tutti il JS
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/custom.js', array(), '1.0.0', true );

  // Carico i JS e i CSS aggiuntivi in base alle opzioni
  if ($GLOBALS['assets_options']['AOS']) {
    wp_enqueue_style( 'aos', get_template_directory_uri() . '/assets/includes/css/aos.css' );
    wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/assets/includes/js/aos.js' );
	if ( !function_exists( 'aos_js' ) ) {
		function aos_js(){ ?>
		  <script>
			AOS.init();
		  </script>
		<?php }
	}
    add_action('wp_footer', 'aos_js', 20);
    
  }
  if ($GLOBALS['assets_options']['ANIMATECSS']) {
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/includes/css/animate.css' );
  }
  if ($GLOBALS['assets_options']['HOVERCSS']) {
    wp_enqueue_style( 'hover', get_template_directory_uri() . '/assets/includes/css/hover.css' );
  }
  if ($GLOBALS['assets_options']['FONTAWESOME']) {
    wp_enqueue_script(  'awesome-js', 'https://use.fontawesome.com/releases/v5.2.0/js/all.js' );
    wp_enqueue_style(  'awesome-css', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css' );
  }

  // Carico la compatibilità con IE9 solo su IE9
  wp_enqueue_script( 'ie9', get_template_directory_uri() . '/assets/includes/js/ie9.js', array(), '3.7.2', false );
  add_filter( 'script_loader_tag', function( $tag, $handle ) {
    if ( $handle === 'ie9' ) {
      $tag = "<!--[if lt IE 9]>$tag<![endif]-->";
    }
    return $tag;
  }, 10, 2 );

}

add_action( 'wp_enqueue_scripts', 'mad_scripts', 100 );


// http://wordpress.stackexchange.com/a/12450
// Gestisco un sostituto per JQuery nel caso in cui il primo sia Offline
function mad_jquery_local_fallback($src, $handle = null) {
  static $add_jquery_fallback = false;

  if ($add_jquery_fallback) {
    echo '<script>window.jQuery || document.write(\'<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>\')</script>' . "\n";
    $add_jquery_fallback = false;
  }

  if ($handle === 'jquery') {
    $add_jquery_fallback = true;
  }

  return $src;
}
add_action('wp_head', 'mad_jquery_local_fallback');

/**
 * Analytics script
 */
function mad_google_analytics() { ?>
  <?php
  $facebook_pixel = get_option('facebook_pixel');
  $shinystat = get_option('seo_shinystat');
  ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo GOOGLE_ANALYTICS_ID ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', '<?php echo GOOGLE_ANALYTICS_ID ?>', { 'anonymize_ip': true });

          <?php if(HOTJAR_ANALYTICS_ID){ ?>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:<?php echo HOTJAR_ANALYTICS_ID ?>,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
          <?php }; ?>
          <?php if($facebook_pixel !== ""){ ?>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo $facebook_pixel; ?>');
            fbq('track', 'PageView');
          <?php }; ?>
          <?php if($shinystat !== ""){ ?>
            var _user = "<?php echo $shinystat; ?>";
            var _sscode=document.createElement('script');
            _sscode.type='text/javascript';
            _sscode.async=true;
            _sscode.src="//codicebusiness.shinystat.com/cgi-bin/getcod.cgi?USER="+_user+"&NODW=yes&RM="+Math.round(Math.random()*2147483647);
            _sscode.onload=function(){return;}
            document.getElementsByTagName('head')[0].appendChild( _sscode);   
          <?php }; ?>

    </script>
<?php }
if (GOOGLE_ANALYTICS_ID) {
  add_action('wp_head', 'mad_google_analytics', 29);
}
$facebook_pixel = get_option('facebook_pixel');
if($facebook_pixel !== ""){
  function add_facebook_pixel() {
    $facebook_pixel = get_option('facebook_pixel');
    echo '<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id='.$facebook_pixel.'&amp;ev=PageView&amp;noscript=1"/></noscript>';
  }
  add_action('wp_footer', 'add_facebook_pixel', 20);
}
function gdpr_setting(){ ?>
	<?php

	$cookie = get_option('enable_cookie');
	$cookie_text = get_option('cookie_text');
    $cookie_accept_text = get_option('cookie_accept_text');

    if($cookie){
        wp_enqueue_script(  'cookieconsent.min.js', get_template_directory_uri().'/assets/includes/js/cookieconsent.min.js', [], "1.0", true );

	    ?>
        <script type="text/javascript">
            window.cookieconsent_options = {"message":"<?=$cookie_text?>","dismiss":"<?=$cookie_accept_text?>","theme": false};
        </script>
	    <?php
    }
	?>
<?php }
add_action('wp_footer', 'gdpr_setting');


/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rd_duplicate_post_as_draft(){
  global $wpdb;
  if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
    wp_die('No post to duplicate has been supplied!');
  }

  /*
 * Nonce verification
 */
  if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
    return;

  /*
 * get the original post id
 */
  $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
  /*
 * and all the original post data then
 */
  $post = get_post( $post_id );

  /*
 * if you don't want current user to be the new post author,
 * then change next couple of lines to this: $new_post_author = $post->post_author;
 */
  $current_user = wp_get_current_user();
  $new_post_author = $current_user->ID;

  /*
 * if post data exists, create the post duplicate
 */
  if (isset( $post ) && $post != null) {

    /*
 * new post data array
 */
    $args = array(
      'comment_status' => $post->comment_status,
      'ping_status'    => $post->ping_status,
      'post_author'    => $new_post_author,
      'post_content'   => $post->post_content,
      'post_excerpt'   => $post->post_excerpt,
      'post_name'      => $post->post_name,
      'post_parent'    => $post->post_parent,
      'post_password'  => $post->post_password,
      'post_status'    => 'draft',
      'post_title'     => $post->post_title,
      'post_type'      => $post->post_type,
      'to_ping'        => $post->to_ping,
      'menu_order'     => $post->menu_order
    );

    /*
 * insert the post by wp_insert_post() function
 */
    $new_post_id = wp_insert_post( $args );

    /*
 * get all current post terms ad set them to the new post draft
 */
    $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
    foreach ($taxonomies as $taxonomy) {
      $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
      wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
    }

    /*
 * duplicate all post meta just in two SQL queries
 */
    $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
    if (count($post_meta_infos)!=0) {
      $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
      foreach ($post_meta_infos as $meta_info) {
        $meta_key = $meta_info->meta_key;
        if( $meta_key == '_wp_old_slug' ) continue;
        $meta_value = addslashes($meta_info->meta_value);
        $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
      }
      $sql_query.= implode(" UNION ALL ", $sql_query_sel);
      $wpdb->query($sql_query);
    }


    /*
 * finally, redirect to the edit post screen for the new draft
 */
    wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
    exit;
  } else {
    wp_die('Post creation failed, could not find original post: ' . $post_id);
  }
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );

/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
  if (current_user_can('edit_posts')) {
    $actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
  }
  return $actions;
}

add_filter('page_row_actions', 'rd_duplicate_post_link', 10, 2);
add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );
?>