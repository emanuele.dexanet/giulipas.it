<?php
if ( function_exists( 'yoast_breadcrumb' ) ) {
    $yoast = $context['breadcrumbs'] = yoast_breadcrumb('<span class="breadcrumbs">','</span>', false );
}
$GLOBALS['context'] = array(
    'content'   		=> get_the_content(),
    'title'     		=> get_the_title(),
    'post_class'		=> get_post_class()[0],
    'time'				=> get_the_time('c'),
    'date'				=> get_the_date(),
    'author_url'		=> get_author_posts_url(get_the_author_meta('ID')),
    'author'			=> get_the_author(),
    /* Twig breadcrumbs -> stampa tra header e content guarda extras.php R.:81 */
    //'breadcrumb_wp' 	=> breadcrumbs(),
    'breadcrumb_yoast'  => $yoast, // Attiva breadcrumbs nel plugin
    /*** URL Base Sito ***/
    'website_root'      => get_site_url(),
    /*** ***/
    // Dexanet Theme Options
    'dex_url_facebook'		=> get_option('facebook_url'),
    'dex_url_twitter'		=> get_option('twitter_url'),
    'dex_url_instagram'		=> get_option('instagram_url'),
    'dex_url_googleplus'	=> get_option('googleplus_url'),

	'dex_img_logo'				=> get_option('header_logo'),
	'dex_img_logo_secondary'	=> get_option('header_logo_secondary'),
	'dex_alt_logo'				=> get_option('header_logo_alt'),

	'dex_rs'				=> get_option('contact_ragione_sociale'),
	'dex_indirizzo'			=> get_option('contact_indirizzo'),
	'dex_telefono'			=> get_option('contact_telefono'),
    'dex_fax'               => get_option('contact_fax'),
	'dex_mail'				=> get_option('contact_mail')
	////////////////////////////////////////////
  );
?>
