<!--Use content to print content default wordpress page-->
	<!--
		{{breadcrumb_yoast}} or {{breadcrumb_wp}}
		{{content}}
	-->
<!---->
<div class="jumbotron jumbotron-fluid bg-primary">
		<div class="container">
			<h1 class="display-3" style="color:#fff;"><i class="fa fa-user"></i> A Headline That Really Means Something</h1>
			<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
			<p><a class="btn btn-secondary btn-lg" href="javascript:$.gdprcookie.display()" role="button">Open Cookie <i class="fa fa-angle-double-right"></i></a></p>
		</div>
	</div>

	<div class="container">
		<!-- Example row of columns -->
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<img class="card-img-top img-fluid" src="{{website_root}}/wp-content/themes/dexanet-theme/screenshot.png" alt="photo">
					<div class="card-body">
						<h4 class="card-title">People of the Earth</h4>
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					</div>
					<div class="card-footer">
						<a href="#" class="btn btn-danger"><i class="fa fa-eye"></i> View</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="card">
					<img class="card-img-top img-fluid" src="{{website_root}}/wp-content/themes/dexanet-theme/screenshot.png" alt="photo">
					<div class="card-body">
						<h4 class="card-title">Hedgehog Dreams</h4>
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					</div>
					<div class="card-footer">
						<a href="#" class="btn btn-danger"><i class="fa fa-eye"></i> View</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<img class="card-img-top img-fluid" src="{{website_root}}/wp-content/themes/dexanet-theme/screenshot.png" alt="photo">
					<div class="card-body">
						<h4 class="card-title">The Boy &amp; His Father</h4>
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					</div>
					<div class="card-footer">
						<a href="#" class="btn btn-danger"><i class="fa fa-eye"></i> View</a>
					</div>
				</div>
			</div>
		</div>
		<hr>
	</div>