<article class="{{post_class}}">
    <header>
      <h1 class="entry-title"> {{title}} </h1>
     	{% include "entry-meta.twig.php" %}
    </header>
    <div class="entry-content">
       {{content}}
    </div>
 </article>
 