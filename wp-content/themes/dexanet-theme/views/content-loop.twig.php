{% for post in posts %}
    <li><a href="{{ post.link }}">{{ post.title }}</a> - {{post.post_content}}</li>
    <img src="{{post.thumbnail.src}}" alt="Thumbnail for Timber" />
{% else %}
        <li>Sorry, no posts matched your criteria</li>
{% endfor %}





<!-- 
ID             // ID of the post
post_author         // ID of the post author
modified_author     // ID of the post author who modified it
post_date           // timestamp in local time
post_date_gmt       // timestamp in gmt time
post_content        // Full (unprocessed) body of the post
post_title          // title of the post
post_excerpt        // excerpt field of the post, caption if attachment
post_status         // post status: publish, new, pending, draft, auto-draft, future, private, inherit, trash
comment_status      // comment status: open, closed
ping_status         // ping/trackback status
post_password       // password of the post
post_name           // post slug, string to use in the URL
post_modified       // timestamp in local time
post_modified_gmt   // timestamp in gmt time
post_parent         // id of the parent post.
guid                // global unique id of the post
menu_order          // menu order
post_type           // type of post: post, page, attachment, or custom string
post_mime_type      // mime type for attachment posts
comment_count       // number of comments
terms               // taxonomy terms
post.get_field('attributo')  // Advanced custom field
-->