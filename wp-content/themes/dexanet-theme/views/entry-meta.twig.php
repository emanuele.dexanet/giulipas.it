<time class="updated" datetime="{{time}}">
{{date}}
</time>

<p class="byline author vcard">
{{__('By', 'mad')}}
<a href="{{author_url}}" rel="author" class="fn">
{{author}}
</a>
</p>
