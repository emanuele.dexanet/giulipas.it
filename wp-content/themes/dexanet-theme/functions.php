<?php

// Load Composer dependencies.
/* require_once __DIR__ . '/vendor/autoload.php';

$timber = new Timber\Timber();
 */
/*-----------------------------------------------------------------------------------*/
/*  Dexanet theme options
/*-----------------------------------------------------------------------------------*/
if (is_admin()) {
  require_once (get_template_directory() . '/theme-options/theme-options.php');
}
/*-----------------------------------------------------------------------------------*/
/*  Dexanet core
/*-----------------------------------------------------------------------------------*/
/**
 * Theme setting
 *
 * The $mad_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$mad_includes = array(
  'plugins-activation/plugins-activation.php', // Plugins
  'core/utils.php',           // Utility functions
  'core/init.php',            // Initial theme setup and constants
  'core/wrapper.php',         // Theme wrapper class
  'core/config.php',          // Configuration
  'core/activation.php',      // Theme activation
  'core/titles.php',          // Page titles
  'core/nav.php',             // Custom nav modifications
  'core/scripts.php',         // Scripts and stylesheets
  'core/write.php',           // Writing something on files
  'core/extras.php',          // Custom functions
  //'core/dashboard.php',       // Custom dashboard for clients
);

if (class_exists('Timber')) {
  Timber::$cache = true;
}

foreach ($mad_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'mad'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);
function custom_pagination($numpages = '', $pagerange = '', $paged = '')
{

  if (empty($pagerange)) {
    $pagerange = 2;
  }
  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if (!$numpages) {
      $numpages = 1;
    }
  }
  /** 
   *  
   */
  /***
   * We construct the pagination arguments to enter into our paginate_links function.
   ***/
  $pagination_args = array(
    'base' => get_pagenum_link(1) . '%_%',
    'format' => 'page/%#%',
    'total' => $numpages,
    'current' => $paged,
    'show_all' => False,
    'end_size' => 1,
    'mid_size' => $pagerange,
    'prev_next' => True,
    'prev_text' => __('&laquo;'),
    'next_text' => __('&raquo;'),
    'type' => 'plain',
    'add_args' => false,
    'add_fragment' => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
    echo $paginate_links;
    echo "</nav>";
  }
}
/***
 * Disable default media organization Media
 ***/
$uploads_use_yearmonth_folders = get_option('uploads_use_yearmonth_folders', 1);
if ($uploads_use_yearmonth_folders) {
  update_option('uploads_use_yearmonth_folders', 0);
}
/***
 * Info Dasboard
 ***/
add_action('wp_dashboard_setup', 'dexanet_dashboard_widgets');
function dexanet_dashboard_widgets()
{
  global $wp_meta_boxes;
  wp_add_dashboard_widget('custom_help_widget', 'Dexanet theme', 'custom_dashboard_help');
}
function custom_dashboard_help()
{
  echo '<p>
        Benvenuto in Dexanet Theme | EU GDPR Ready.<br> 
        Sviluppato in <a target="_blank" href="http://getbootstrap.com/">Bootstrap 4.</a><br>
        Framework: <a target="_blank" href="https://timber.github.io/docs/">Timber</a><br>
        Info: <a href="mailto:assistenza@dexanet.com">assistenza@dexanet.com</a>
      </p>
      <img style="width:100%;" src="' . get_template_directory_uri() . '/logo.png" alt="">';
}
// Footer widget
function arphabet_widgets_init()
{
  register_sidebar(
    array(
      'name' => 'Footer 1',
      'id' => 'footer_bottom',
      'before_widget' => '<div>',
      'after_widget' => '</div>',
      'before_title' => '<h2>',
      'after_title' => '</h2>',
    )
  );
  register_sidebar(
    array(
      'name' => 'Footer 2',
      'id' => 'footer_bottom_2',
      'before_widget' => '<div>',
      'after_widget' => '</div>',
      'before_title' => '<h2>',
      'after_title' => '</h2>',
    )
  );
  register_sidebar(
    array(
      'name' => 'Footer 3',
      'id' => 'footer_bottom_3',
      'before_widget' => '<div>',
      'after_widget' => '</div>',
      'before_title' => '<h2>',
      'after_title' => '</h2>',
    )
  );
  register_sidebar(
    array(
      'name' => 'Footer 4',
      'id' => 'footer_bottom_4',
      'before_widget' => '<div>',
      'after_widget' => '</div>',
      'before_title' => '<h2>',
      'after_title' => '</h2>',
    )
  );
}
add_action('widgets_init', 'arphabet_widgets_init');
// Use this widget code on template page to show widget content
// Sono già inseriti nel footer
/*
<?php if ( is_active_sidebar( 'footer_bottom' ) ) : ?>
  <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
    <?php dynamic_sidebar( 'footer_bottom' ); ?>
  </div><!-- #footer area -->
<?php endif; ?>
*/
function get_breadcrumb_yoast()
{
  yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
}
/*** Secondary menu ***/
function register_my_menus()
{
  register_nav_menus(
    array(
      'menu_1' => __('Menu 1'),
      'menu_2' => __('Menu 2')
    )
  );
}
add_action('init', 'register_my_menus');
/** Aggiungi questo php per stampare il menu nel file frontend **/
//wp_nav_menu( array( 'theme_location' => 'menu_1', 'container_class' => 'my_extra_menu_class' ) );

/***
 * Connessione server SMTP
 ***/
$enable_smtp = get_option('enable_smtp');

if ($enable_smtp == 1) {
  add_action('phpmailer_init', 'send_smtp_email');
  function send_smtp_email($phpmailer)
  {
    $SMTP_HOST = get_option('host_smtp');
    $SMTP_AUTH = get_option('auth_smtp');
    $SMTP_PORT = get_option('port_smtp');
    $SMTP_USER = get_option('user_smtp');
    $SMTP_PASS = get_option('pass_smtp');
    $SMTP_SECURE = get_option('secure_smtp');
    $SMTP_FROM = get_option('from_smtp');
    $SMTP_NAME = get_option('name_smtp');

    $phpmailer->isSMTP();
    $phpmailer->Host = "$SMTP_HOST";
    $phpmailer->SMTPAuth = $SMTP_AUTH;
    $phpmailer->Port = "$SMTP_PORT";
    $phpmailer->Username = "$SMTP_USER";
    $phpmailer->Password = "$SMTP_PASS";
    $phpmailer->SMTPSecure = "$SMTP_SECURE";
    $phpmailer->From = "$SMTP_FROM";
    $phpmailer->FromName = "$SMTP_NAME";
    $phpmailer->SMTPAutoTLS = false;
  }
}
/***
 * Sicurezza
 ***/
remove_action('wp_head', 'wp_generator');
function wp_remove_version()
{
  return '';
}
add_filter('the_generator', 'wp_remove_version');
// Rimuove i commenti di yost nel codice es.: Power by yoast ecc...
add_action('wp_head', function () {
  ob_start(function ($o) {
    return preg_replace('/^\n?<!--.*?[Y]oast.*?-->\n?$/mi', '', $o);
  });
}, ~PHP_INT_MAX);
// Rimuove ?ver=1.0.0 da css e js
function at_remove_wp_ver_css_js($src)
{
  if (strpos($src, 'ver='))
    $src = remove_query_arg('ver', $src);
  return $src;
}
add_filter('style_loader_src', 'at_remove_wp_ver_css_js', 9999);
add_filter('script_loader_src', 'at_remove_wp_ver_css_js', 9999);
/***
 * Riduzione del numero di immagini generate da wordpress upload media
 ***/
function wpmayor_filter_image_sizes($sizes)
{
  unset($sizes['medium']);          // Remove Thumbnail (150 x 150 hard cropped)
  unset($sizes['medium']);          // Remove Medium resolution (300 x 300 max height 300px)
  unset($sizes['medium_large']);    // Remove Medium Large (added in WP 4.4) resolution (768 x 0 infinite height)
  unset($sizes['large']);           // Remove Large resolution (1024 x 1024 max height 1024px)
  /* With WooCommerce */
  unset($sizes['shop_thumbnail']);  // Remove Shop thumbnail (180 x 180 hard cropped)
  unset($sizes['shop_catalog']);    // Remove Shop catalog (300 x 300 hard cropped)
  unset($sizes['shop_single']);     // Shop single (600 x 600 hard cropped)
  return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'wpmayor_filter_image_sizes');
/*** 
 * SVG abilitate upload media
 * ***/
function cc_mime_types($mimes)
{
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
/*** 
 * Dexanet Backend Editor
 ***/
function admin_style()
{
  wp_enqueue_style('admin-styles', get_template_directory_uri() . '/theme-options/admin/scss/admin-style.css');
}
add_action('admin_enqueue_scripts', 'admin_style');
function my_login_stylesheet()
{
  wp_enqueue_style('custom-login', get_template_directory_uri() . '/theme-options/admin/scss/login.css');
}
add_action('login_enqueue_scripts', 'my_login_stylesheet');
/***
 * Dexanet Login
 ***/
function my_login_logo()
{ ?>
  <style type="text/css">
    #login h1 a,
    .login h1 a {
      background-image: url(<?php echo get_template_directory_uri(); ?>/theme-options/admin/images/logo-piccolo.svg);
      height: 65px;
      width: 320px;
      background-size: 320px 90px;
      background-repeat: no-repeat;
      padding-bottom: 30px;
    }
  </style>
<?php }
add_action('login_enqueue_scripts', 'my_login_logo');
/* -------- */
/*** 
 * Custom API endpoint
 * permette di generare un URL come endpoint della chiamata API  WordPress Version > 4.9.0
 ***/
/** 
 * File del tuo array che verra codificato in JSON 
 * Decommenta per utilizzarlo! 
 ***/
// require_once (get_template_directory() . '/api/json.php');
/*** 
 * Registro la root dell'endpoint REST API
 * Decommenta per utilizzare!
 ***/
/*function prefix_register_example_routes() {
  register_rest_route( 'dex/v1', '/phrase', array(
      'methods'  => WP_REST_Server::READABLE,
      'callback' => 'prefix_get_endpoint_phrase',
  ) );
}
add_action( 'rest_api_init', 'prefix_register_example_routes' );*/
/* -------- */

add_action(
  'wpcf7_before_send_mail',
  function ($contact_form, &$abort, $submission) {
    foreach ($submission->uploaded_files() as $file_uploaded) {
      if (!empty ($file_uploaded)) {
        $filesize = filesize($file_uploaded[0]);
        if ($filesize == 0) {
          $abort = true;
          $submission->set_response(wpcf7_get_message('upload_failed'));
        }
      }
    }

  },
  10,
  3
);

// REFERER PAGE FOR CONTACT FORM
function getRefererPage($form_tag)
{
  if (isset($_SERVER['HTTP_REFERER'])) {
    $url = htmlspecialchars($_SERVER['HTTP_REFERER']);

    if ($form_tag['name'] == 'referer-page') {
      $form_tag['type'] = 'hidden';
      $form_tag['basetype'] = 'hidden';
      $form_tag['values'][] = $url;
    }

  }

  return $form_tag;
}

if (!is_admin()) {
  add_filter('wpcf7_form_tag', 'getRefererPage');
}