<?php
/*
Template Name: Loop Example
*/
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'loop'); ?>
<?php endwhile; ?>

<?php //$loop = new WP_Query( array( 'post_type' => 'yourposttypehere', 'posts_per_page' => -1 ) ); ?>
<?php //while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php //endwhile; wp_reset_query(); ?>
