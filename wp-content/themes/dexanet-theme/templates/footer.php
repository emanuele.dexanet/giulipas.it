<?php 
    $layout_menu = get_option('footer_layout'); 
    $footer_text = get_option('footer_text'); 
?>
<footer class="content-info" role="contentinfo">
  <div class="<?php
      if ($layout_menu == 0){
        echo 'container'; 
        } else {
        echo 'container-fluid';
      }
    ?>">
    <div class="row">
    	<div class="col">
    		<?php if ( is_active_sidebar( 'footer_bottom' ) ) : ?>
			  <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
			    <?php dynamic_sidebar( 'footer_bottom' ); ?>
			  </div><!-- #footer area -->
			<?php endif; ?>
    	</div>
    	<div class="col">
    		<?php if ( is_active_sidebar( 'footer_bottom_2' ) ) : ?>
			  <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
			    <?php dynamic_sidebar( 'footer_bottom' ); ?>
			  </div><!-- #footer area -->
			<?php endif; ?>
    	</div>
    	<div class="col">
    		<?php if ( is_active_sidebar( 'footer_bottom_3' ) ) : ?>
			  <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
			    <?php dynamic_sidebar( 'footer_bottom' ); ?>
			  </div><!-- #footer area -->
			<?php endif; ?>
    	</div>
    	<div class="col">
    		<?php if ( is_active_sidebar( 'footer_bottom_4' ) ) : ?>
			  <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
			    <?php dynamic_sidebar( 'footer_bottom' ); ?>
			  </div><!-- #footer area -->
			<?php endif; ?>
    	</div>
    </div>
    <?php //dynamic_sidebar('sidebar-footer'); ?>
    <div class="row">
        <?php echo $footer_text; ?>
    </div>
  </div>
</footer>


