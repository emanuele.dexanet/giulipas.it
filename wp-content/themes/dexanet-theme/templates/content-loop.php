<?php

$context = Timber::get_context();

$args = array(
	'post_type' => 'loop', // Nome del custom post
    'posts_per_page' => -1,
    'orderby' => array(
        'date' => 'DESC'
    ),

);

$posts = Timber::get_posts( $args );
$context['posts'] = $posts;

Timber::render( 'content-loop.twig.php', $context );
