<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="it"> <!--<![endif]-->
    <head>
        <?php
            $google_font = get_option('googlefont_url');
            $google_font_2 = get_option('googlefont_2_url');
            $google_font_3 = get_option('googlefont_3_url');
            $status_bar = get_option('statusbar_color');
        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <!-- Pingback URL -->
        <link rel="pingback" href="xmlrpc.php" />
        <link rel="author" href="humans.txt" />
        <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">
        
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-180x180.png">

        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="36x36"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-36x36.png">
        <link rel="icon" type="image/png" sizes="48x48"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-48x48.png">
        <link rel="icon" type="image/png" sizes="72x72"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-72x72.png">
        <link rel="icon" type="image/png" sizes="96x96"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-96x96.png">
        <link rel="icon" type="image/png" sizes="144x144"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-144x144.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-192x192.png">

        <!--Select mobile color status bar-->
        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="<?=$status_bar?>" />
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="<?=$status_bar?>" />
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        
        <?php
            if($google_font !== ""){
                echo '<link href="'.$google_font.'" rel="stylesheet">';
            }
        ?>
        <?php
            if($google_font_2 !== ""){
                echo '<link href="'.$google_font_2.'" rel="stylesheet">';
            }
        ?>
        <?php
            if($google_font_3 !== "") {
                echo '<link href="' . $google_font_3 . '" rel="stylesheet">';
            }
        ?>
        <?php wp_head(); ?>     
    </head>