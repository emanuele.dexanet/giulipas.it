<?php 
    $layout_menu = get_option('menu_layout');
    $logo_brand = get_option('header_logo');
    $logo_brand_alt = get_option('header_logo_alt');
    $menu_position = get_option('menu_position_settings');
?>
<header class="" role="banner">
  <div class="<?php
      if ($layout_menu == 0){
        echo 'container';
        } else {
        echo '';
      }
    ?>">
    <nav class="navbar <?=$menu_position['menu_position_select_field_0']?> navbar-expand-lg navbar-light bg-light bg-faded">
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
            <?php
            if ($logo_brand == ""){
                bloginfo('name');
            } else {
                echo '<img class="logo-nav" src="'.$logo_brand.'" alt="'.$logo_brand_alt.'">';
            }
            ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'navbar-nav mr-auto'));
          endif;
        ?>
      </div>
    </nav>
  </div>
</header>