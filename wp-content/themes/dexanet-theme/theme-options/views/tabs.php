<h2 class="nav-tab-wrapper">
    <a href="?page=theme-options&tab=theme-options" class="nav-tab <?php if($active_tab == 'theme-options'){echo 'nav-tab-active';} ?>">
        <?php _e('Theme options', 'sandbox'); ?>
    </a>
    <a href="?page=theme-options&tab=seo-options" class="nav-tab <?php if($active_tab == 'seo-options'){echo 'nav-tab-active';} ?> ">
        <?php _e('Seo options', 'sandbox'); ?>
    </a>
    <a href="?page=theme-options&tab=social-options" class="nav-tab <?php if($active_tab == 'social-options'){echo 'nav-tab-active';} ?>">
        <?php _e('Social options', 'sandbox'); ?>
    </a>
    <a href="?page=theme-options&tab=google-options" class="nav-tab <?php if($active_tab == 'google-options'){echo 'nav-tab-active';} ?>">
        <?php _e('Google options', 'sandbox'); ?>
    </a>
    <a href="?page=theme-options&tab=contact-options" class="nav-tab <?php if($active_tab == 'contact-options'){echo 'nav-tab-active';} ?>">
        <?php _e('General contact', 'sandbox'); ?>
    </a>
    <a href="?page=theme-options&tab=footer-options" class="nav-tab <?php if($active_tab == 'footer-options'){echo 'nav-tab-active';} ?>">
        <?php _e('Footer options', 'sandbox'); ?>
    </a>
    <a href="?page=theme-options&tab=cookie-options" class="nav-tab <?php if($active_tab == 'cookie-options'){echo 'nav-tab-active';} ?>">
        <?php _e('Cookie options', 'sandbox'); ?>
    </a>
    <a href="?page=theme-options&tab=email-smtp" class="nav-tab <?php if($active_tab == 'email-smtp'){echo 'nav-tab-active';} ?>">
        <?php _e('E-Mail SMTP', 'sandbox'); ?>
    </a>
</h2>
