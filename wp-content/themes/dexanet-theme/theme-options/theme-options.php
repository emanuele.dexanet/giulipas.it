<?php
    function add_new_menu_items(){
        add_menu_page(
            "Dexanet theme",
            "Dexanet theme",
            "manage_options",
            "theme-options",
            "theme_options_page",
            get_template_directory_uri().'/theme-options/deXnet.svg', 
            100 
        );
        add_submenu_page(
            "theme-options",
            "Seo options",
            "Seo options",
            "manage_options",
            "?page=theme-options&tab=seo-options"
        );
        add_submenu_page(
            "theme-options",
            "Social options",
            "Social options",
            "manage_options",
            "?page=theme-options&tab=social-options"
        );
        add_submenu_page(
            "theme-options",
            "Google options",
            "Google options",
            "manage_options",
            "?page=theme-options&tab=google-options"
        );
        add_submenu_page(
            "theme-options",
            "Contact options",
            "Contact options",
            "manage_options",
            "?page=theme-options&tab=contact-options"
        );
        add_submenu_page(
            "theme-options",
            "Footer options",
            "Footer options",
            "manage_options",
            "?page=theme-options&tab=footer-options"
        );
        add_submenu_page(
            "theme-options",
            "Cookie options",
            "Cookie options",
            "manage_options",
            "?page=theme-options&tab=cookie-options"
        );
        add_submenu_page(
            "theme-options",
            "Email SMTP",
            "Email SMTP",
            "manage_options",
            "?page=theme-options&tab=email-smtp"
        );
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Barra SEO Footer',
                'menu_title'	=> 'Barra SEO Footer',
                'parent_slug'	=> 'theme-options',
            ));
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Campi personalizzati generali',
                'menu_title'	=> 'Archivi & altro',
                'parent_slug'	=> 'theme-options',
            ));
            acf_add_options_sub_page(array(
                'page_title' 	=> 'Traduzioni',
                'menu_title'	=> 'Traduzioni',
                'parent_slug'	=> 'theme-options',
            ));
        }
    }
    function theme_options_page(){
        ?>
        <div class="wrap">
            <div id="icon-options-general" class="icon32"></div>
            <!-- run the settings_errors() function here. -->
            <?php settings_errors(); ?>
            <img style="width:350px;" src="<?php echo get_template_directory_uri().'/theme-options/brand-Theme.svg' ?>" alt="Dexanet">
            <?php require_once ( get_template_directory() . '/theme-options/models/selectable-tabs.php' );?>
            <?php require_once ( get_template_directory() . '/theme-options/views/tabs.php' );?>
            <form method="post" action="options.php" enctype="multipart/form-data">
                <?php require_once ( get_template_directory() . '/theme-options/controller/save.php' );?>
            </form>
        </div>
        <?php
    }
    add_action("admin_menu", "add_new_menu_items");
    function display_options(){
        if(isset($_GET["tab"])){
            if($_GET["tab"] == "theme-options"){
                require_once ( get_template_directory() . '/theme-options/models/theme-options.php' );
            }
            elseif($_GET["tab"] == "seo-options"){
                require_once ( get_template_directory() . '/theme-options/models/seo-options.php' );
            }
            elseif($_GET["tab"] == "social-options"){
                require_once ( get_template_directory() . '/theme-options/models/social-options.php' );
            }
            elseif($_GET["tab"] == "google-options"){
                require_once ( get_template_directory() . '/theme-options/models/google-options.php' );
            }
            elseif($_GET["tab"] == "contact-options"){
                require_once ( get_template_directory() . '/theme-options/models/contact-options.php' );
            }
            elseif($_GET["tab"] == "cookie-options"){
                require_once ( get_template_directory() . '/theme-options/models/cookie-options.php' );
            }elseif($_GET["tab"] == "footer-options"){
                require_once ( get_template_directory() . '/theme-options/models/footer-options.php' );
            }elseif($_GET["tab"] == "email-smtp"){
                require_once ( get_template_directory() . '/theme-options/models/email-smtp.php' );
            }
        }else{
            require_once ( get_template_directory() . '/theme-options/models/theme-options.php' );
            require_once ( get_template_directory() . '/theme-options/models/register-setting.php' );
        }
    }
    require_once ( get_template_directory() . '/theme-options/models/header-info.php' );
    require_once ( get_template_directory() . '/theme-options/views/input.php' );
    add_action("admin_init", "display_options");
