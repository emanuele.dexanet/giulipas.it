<?php
if(isset($_GET["tab"])){
	if($_GET["tab"] == "theme-options"){
		settings_fields("header_section");
	} elseif($_GET["tab"] == "seo-options"){
		settings_fields("seo_section");
	} elseif($_GET["tab"] == "social-options"){
		settings_fields("social_section");
	} elseif($_GET["tab"] == "google-options"){
		settings_fields("google_section");
	} elseif($_GET["tab"] == "contact-options"){
		settings_fields("contact_section");
	} elseif($_GET["tab"] == "footer-options"){
		settings_fields("footer_section");
	} elseif($_GET["tab"] == "cookie-options"){
		settings_fields("cookie_section");
	}  elseif($_GET["tab"] == "email-smtp"){
		settings_fields("email_section");
	}
}
else {
	settings_fields("header_section");
}
do_settings_sections("theme-options");
submit_button();
?>