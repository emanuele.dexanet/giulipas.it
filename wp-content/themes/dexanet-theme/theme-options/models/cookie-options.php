<?php 
    add_settings_section("cookie_section", "Impostazioni", "display_cookie_header", "theme-options");
    // Cookie Options
	add_settings_field(
		"enable_cookie",
		"Avviso cookie",
		"display_cookie",
		"theme-options",
		"cookie_section"
	);
    add_settings_field(
        "cookie_text",
        "Inserisci il testo dei cookie",
        "display_cookie_text",
        "theme-options",
        "cookie_section"
    );
    add_settings_field(
        "cookie_accept_text",
        "Inserisci il testo del pulsante",
        "display_cookie_accept_text",
        "theme-options",
        "cookie_section"
    );
?>