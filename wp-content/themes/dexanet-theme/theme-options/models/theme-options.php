<?php
    add_settings_section("header_section", "Impostazioni", "display_header_options_content", "theme-options");
    // Theme options
    add_settings_field(
        "aos_js",
        "Abilita AOS js",
        "display_layout_element_aos",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "font_awesome",
        "Abilita Font Awesome",
        "display_layout_element_fontawesome",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        'animate_css',
        "Abilita Animate Css",
        "display_layout_element_animatecss",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        'hover_css',
        "Abilita effetti hover (classi css)",
        "display_layout_element_hover_css",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "theme_layout",
        "Vuoi il corpo del sito in full width?",
        "display_layout_element",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "menu_layout",
        "Vuoi il menu in full width?",
        "display_layout_element_menu",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "menu_position_settings",
        "Seleziona la posizione del main menu",
        "menu_position_select_field_0_render",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "footer_layout",
        "Vuoi il footer in full width?",
        "display_layout_element_footer",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "header_logo",
        "Logo Url",
        "display_logo_form_element",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "header_logo_secondary",
        "Logo secondario Url",
        "display_logo_secondary_form_element",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "header_logo_alt",
        "Testo alternativo logo",
        "display_logo_alt_form_element",
        "theme-options",
        "header_section"
    );
    add_settings_field(
        "statusbar_color",
        "Colore Status bar Android",
        "display_color_statusbar",
        "theme-options",
        "header_section"
    );
?>