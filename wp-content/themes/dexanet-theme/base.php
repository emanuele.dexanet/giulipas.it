<?php get_template_part('templates/head'); ?>
 <?php if ( GOOGLE_TAG_MANAGER_ID ) : ?>
      <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','<?php echo GOOGLE_TAG_MANAGER_ID; ?>');
  </script>
  <!-- End Google Tag Manager -->
  <?php endif; ?>   


<?php if ( GOOGLE_TAG_MANAGER_ID ) : ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo GOOGLE_TAG_MANAGER_ID; ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif; ?>   
<?php $layout = get_option('theme_layout'); ?>
<body <?php body_class(); ?>>
<?php do_action('gdpr_set'); ?>
  <?php
    do_action('get_header');
    get_template_part('templates/header');
  ?>
<div class="<?php
  if ($layout == 0){
    echo 'container';
  } else {
    echo 'container-fluid';
  }
?>">
  <div class="wrap" role="document">
    <?php include mad_template_path(); ?>
  </div><!-- /.wrap -->
</div>
  <?php get_template_part('templates/footer'); ?>
  
  <?php wp_footer(); ?>
</body>
</html>