��    %      D  5   l      @     A     I     R     ^     f     r     u     |     �     �  !   �     �     �  
   �     �     �     �                         '  	   0     :     C     Q     c  b   k     �     �     �     �     �     �  3        8  `  P     �  
   �     �     �     �     �     �     �             &   )     P     c     i     u     {     �     �     �  
   �     �     �  	   �  
   �     �     	     !	  q   *	     �	  	   �	     �	     �	     �	     �	  7   �	     
                                                                	   #                
       !                   %              "                                                $        %s says 404 Page Add comment Archive Archive: %s By Cancel Case History Archive Page Comment Comments Comments for this post are closed Credits page Email Filter by: First Last Leave a comment... Loading Name Next No posts found. Password Password: Previous Redirect page Redirect settings Replies Select a page to redirect to when a form submission completes and mail has been sent successfully. Send Share Show all Submit This is my archive Website Your comment will be revised by the site if needed. – Case Histories Page Project-Id-Version: Giulipas
PO-Revision-Date: 2021-06-14 13:03+0200
Last-Translator: 
Language-Team: 
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: ..
X-Poedit-WPHeader: style.css
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 %s dice Pagina 404 Aggiungi un commento Archivio Archivio: %s Di Annulla Pagina Archivio Case History Commento Commenti I commenti per questo post sono chiusi Pagina dei crediti Email Filtra per: Prima Ultima Lascia un commento.... Caricamento Nome Successiva Nessun post trovato. Password Password: Precedente Pagina di reindirizzamento Impostazioni reindirizzamento Risposte Seleziona una pagina a cui reindirizzare quando viene inviato un modulo e l'email è stata inviata correttamente. Invia Condividi Mostra tutti Invia Questo è il mio archivio Sito web Il tuo commento verrà rivisto dal sito, se necessario. - Pagina Case History 