<?php
/**
 * Sets the directories (inside theme) to find .twig files.
 *
 * @since 1.0.0
 */
Timber::$dirname = array( 'templates', 'views' );


/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * Just set this value to true.
 *
 * @since 1.0.0
 */
Timber::$autoescape = false;


/**
 * Configures theme inside of a subclass of Timber\Site.
 *
 * @since 1.0.0
 */
class GiulipasSite extends Timber\Site {
	function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'widgets_init', array( $this, 'register_widgets' ), 11 );
		add_action( 'acf/init', array( $this, 'register_blocks' ) );
		add_action( 'init', array( $this, 'register_block_patterns' ) );

		parent::__construct();
	}


	function theme_setup() {
		if ( ! defined( 'MST_THEME_DIR' ) ) {
			define( 'MST_THEME_DIR', trailingslashit( get_stylesheet_directory() ) );
		}

		if ( ! defined( 'MST_THEME_URL' ) ) {
			define( 'MST_THEME_URL', trailingslashit( get_stylesheet_directory_uri() ) );
		}


		if ( ! defined( 'MST_THEME_DOMAIN' ) ) {
			define( 'MST_THEME_DOMAIN', 'mst' );
		}

		load_child_theme_textdomain( MST_THEME_DOMAIN, MST_THEME_DIR . 'languages' );


		require_once( MST_THEME_DIR . 'inc/theme-support.php' );
		require_once( MST_THEME_DIR . 'inc/theme-options.php' );
		require_once( MST_THEME_DIR . 'libs/Mobile_Detect.php' );
		require_once( MST_THEME_DIR . 'inc/template-functions.php' );
		require_once( MST_THEME_DIR . 'inc/template-tags.php' );
		require_once( MST_THEME_DIR . 'inc/ajax-functions.php' );
		require_once( MST_THEME_DIR . 'inc/menus.php' );
		require_once( MST_THEME_DIR . 'inc/block-scripts-admin.php' );
		require_once( MST_THEME_DIR . 'inc/block-scripts.php' );
		require_once( MST_THEME_DIR . 'inc/wp-scripts-admin.php' );
		require_once( MST_THEME_DIR . 'inc/wp-scripts.php' );
		require_once( MST_THEME_DIR . 'inc/actions.php' );
		require_once( MST_THEME_DIR . 'inc/filters.php' );
		require_once( MST_THEME_DIR . 'inc/acf-functions.php' );
		require_once( MST_THEME_DIR . 'inc/acf-filters.php' );
		require_once( MST_THEME_DIR . 'inc/acf-actions.php' );
		require_once( MST_THEME_DIR . 'inc/cf7-extended.php' );
	}


	/**
	 * Adds some context.
	 *
	 * @param {string} $context : context['this'] being the Twig's {{ this }}.
	 */
	function add_to_context( $context ) {
		$context['site']         = $this;
		$context['primary_menu'] = new Timber\Menu( 'primary_navigation' );


		/* Theme options */
		$context['theme_layout']  = ( 0 == get_option( 'theme_layout' ) ) ? 'container' : 'container-fluid';
		$context['menu_layout']   = ( 0 == get_option( 'menu_layout' ) ) ? 'container' : 'container-fluid';
		$context['menu_position'] = get_option( 'menu_position_settings' )['menu_position_select_field_0'];
		$context['footer_layout'] = ( 0 == get_option( 'footer_layout' ) ) ? 'container' : 'container-fluid';

		$header_logo = array();

		if ( '' != get_option( 'header_logo' ) ) {
			$header_logo['url'] = get_option('header_logo');
		}

		if ( '' != get_option( 'header_logo_secondary' ) ) {
			$header_logo['secondary_url'] = get_option('header_logo_secondary');
		}

		if ( '' != get_option('header_logo_alt') ) {
			$header_logo['alt'] = get_option('header_logo_alt');
		}

		if ( !empty( $header_logo ) ) {
			$context['header_logo'] = $header_logo;
		}

		$context['colorful_logo'] = '<svg class="logo xclss" xmlns="http://www.w3.org/2000/svg" width="292" height="50.177" viewBox="0 0 292 50.177">
			<defs>
				<style>.a{fill:#0090df;}.b{fill:#464545;}</style>
			</defs>
			<path class="a" d="M1071.6,82.982a24.945,24.945,0,1,0,0,49.89h31.484v-31.6h-30.93a6.586,6.586,0,1,0,0,13.171h16a2.2,2.2,0,1,1,0,4.392h-16a10.978,10.978,0,0,1,0-21.956h35.321v35.992h13.836V82.982Z" transform="translate(-1046.656 -82.982)"/><path class="b" d="M1330.706,104.137a4.65,4.65,0,0,1-1.156,3.382,4.916,4.916,0,0,1-3.624,1.177h-5.318a4.967,4.967,0,0,1-3.688-1.177,4.705,4.705,0,0,1-1.135-3.382V84.011h-5.6v20.524c0,6.415,3.3,9.667,9.8,9.667h6.294c3.242,0,5.72-.818,7.365-2.433s2.479-4.049,2.479-7.234V84.011h-5.419Z" transform="translate(-1173.916 -83.479)"/><path class="b" d="M1379.036,108.3a4.833,4.833,0,0,1-1.831-1.054,3.636,3.636,0,0,1-.961-1.58,6.824,6.824,0,0,1-.282-1.976V84.011h-5.507v19.726a13.82,13.82,0,0,0,.6,4.186,8.329,8.329,0,0,0,1.96,3.312,9.2,9.2,0,0,0,3.485,2.184,14.921,14.921,0,0,0,5.123.784h12.06V108.7h-11.75A9.2,9.2,0,0,1,1379.036,108.3Z" transform="translate(-1203.018 -83.479)"/><rect class="b" width="5.508" height="30.191" transform="translate(195.897 0.532)"/><path class="b" d="M1469.315,99.574a8.356,8.356,0,0,0,1.691-3.064,11.8,11.8,0,0,0,.515-3.408,10.757,10.757,0,0,0-.562-3.457,7.727,7.727,0,0,0-1.759-2.91,8.658,8.658,0,0,0-3.04-1.983,11.812,11.812,0,0,0-4.4-.741h-15.427v5.507h15.65a3.766,3.766,0,0,1,3.018,1.125,4.086,4.086,0,0,1,.964,2.724,4.722,4.722,0,0,1-.236,1.485,3.8,3.8,0,0,1-.706,1.262,3.405,3.405,0,0,1-1.235.894,4.565,4.565,0,0,1-1.849.342h-10.77a5.077,5.077,0,0,0-3.687,1.148,5.148,5.148,0,0,0-1.148,3.732V114.2h5.506V103.25c0-.425.19-.615.615-.615h9.307a10.83,10.83,0,0,0,4.512-.855A8.8,8.8,0,0,0,1469.315,99.574Z" transform="translate(-1239.662 -83.479)"/><path class="b" d="M1509.411,83.864a7.986,7.986,0,0,0-3.946-.882,8.092,8.092,0,0,0-3.991.882,5.806,5.806,0,0,0-2.432,3.061l-10.576,26.78h6.06l9.515-24.148a1.4,1.4,0,0,1,1.38-.935,1.318,1.318,0,0,1,1.331.929l9.085,24.064.034.09h6.1L1511.8,86.928A5.609,5.609,0,0,0,1509.411,83.864Z" transform="translate(-1260.005 -82.982)"/><path class="b" d="M1584.123,99.168a8.34,8.34,0,0,0-3.133-2.052,13.905,13.905,0,0,0-5.058-.786h-7a6.714,6.714,0,0,1-2.127-.28,3.325,3.325,0,0,1-1.3-.764,2.663,2.663,0,0,1-.676-1.1,4.3,4.3,0,0,1-.191-1.266,3.467,3.467,0,0,1,.892-2.453,3.779,3.779,0,0,1,2.867-.953h15.473V84.011h-15.295a12.736,12.736,0,0,0-4.3.651,7.834,7.834,0,0,0-2.955,1.828,7,7,0,0,0-1.67,2.78,11.259,11.259,0,0,0-.517,3.476,11.966,11.966,0,0,0,.426,3.138,7.07,7.07,0,0,0,1.535,2.847,7.753,7.753,0,0,0,3.044,2.033,13.762,13.762,0,0,0,4.973.763h6.957a7.068,7.068,0,0,1,2.237.3,3.753,3.753,0,0,1,1.389.789,2.707,2.707,0,0,1,.717,1.139,4.412,4.412,0,0,1,.215,1.353,3.6,3.6,0,0,1-.958,2.585,4.041,4.041,0,0,1-3.025,1h-16.979V114.2h16.8a12.845,12.845,0,0,0,4.415-.673,8.069,8.069,0,0,0,3-1.872,7.27,7.27,0,0,0,1.716-2.823,11.17,11.17,0,0,0,.539-3.546,12.634,12.634,0,0,0-.425-3.226A6.847,6.847,0,0,0,1584.123,99.168Z" transform="translate(-1294.13 -83.479)"/><path class="b" d="M1228.684,101.716h12.61V103.6h0v.7a4.671,4.671,0,0,1-1.216,3.382,4.909,4.909,0,0,1-3.622,1.177h-8.851a10.269,10.269,0,0,1-6.857-2.942c-1.73-1.73-2.3-4.867-2.3-7.315,0-5.888,4.109-9.146,9.16-9.146h19.048V84.081h-19.42a15.019,15.019,0,1,0,0,30.039h5.908v-.005h3.668c3.242,0,5.721-.818,7.365-2.433s2.479-4.049,2.479-7.234V96.394h-17.97Z" transform="translate(-1126.603 -83.513)"/><path class="b" d="M1289.678,84.081h-.755V114.12h5.769V84.081Z" transform="translate(-1163.647 -83.513)"/><path class="a" d="M1411.3,164.008h-1.565l3.5,5.182v2.924h1.364v-2.851l3.557-5.255h-1.566l-2.644,4.12Zm-8.1,4.058h-3.589v.863h2.219v1.839a2.062,2.062,0,0,1-.785.417,4.639,4.639,0,0,1-1.414.183,2.951,2.951,0,0,1-2.02-.7,2.262,2.262,0,0,1-.791-1.776v-1.681a2.363,2.363,0,0,1,.711-1.764,2.557,2.557,0,0,1,1.872-.7,2.882,2.882,0,0,1,1.754.456,1.587,1.587,0,0,1,.673,1.165h1.309l.012-.034a2.108,2.108,0,0,0-1.014-1.764,4.756,4.756,0,0,0-2.734-.685,4.3,4.3,0,0,0-2.855.929,3,3,0,0,0-1.107,2.406v1.67a2.91,2.91,0,0,0,1.188,2.4,4.71,4.71,0,0,0,3,.931,6.064,6.064,0,0,0,2.314-.373,3.181,3.181,0,0,0,1.255-.819Zm-16.031.717a2.422,2.422,0,0,1-.747,1.84,2.816,2.816,0,0,1-2.023.711,2.576,2.576,0,0,1-1.908-.712,2.481,2.481,0,0,1-.715-1.839v-1.453a2.471,2.471,0,0,1,.715-1.826,2.575,2.575,0,0,1,1.908-.711,2.822,2.822,0,0,1,2.02.711,2.407,2.407,0,0,1,.75,1.826Zm1.371-1.441a3.111,3.111,0,0,0-1.15-2.469,4.445,4.445,0,0,0-2.991-.982,4.186,4.186,0,0,0-2.883.985,3.152,3.152,0,0,0-1.11,2.467v1.441a3.147,3.147,0,0,0,1.11,2.468,4.208,4.208,0,0,0,2.883.979,4.47,4.47,0,0,0,2.991-.979,3.11,3.11,0,0,0,1.15-2.468Zm-19.095-3.334h-1.369v8.106h6.165v-.863h-4.8Zm-10.059,4.775a2.424,2.424,0,0,1-.748,1.84,2.816,2.816,0,0,1-2.023.711,2.582,2.582,0,0,1-1.91-.712,2.48,2.48,0,0,1-.713-1.839v-1.453a2.47,2.47,0,0,1,.713-1.826,2.581,2.581,0,0,1,1.91-.711,2.816,2.816,0,0,1,2.019.711,2.4,2.4,0,0,1,.752,1.826Zm1.371-1.441a3.109,3.109,0,0,0-1.154-2.469,4.437,4.437,0,0,0-2.988-.982,4.2,4.2,0,0,0-2.885.985,3.159,3.159,0,0,0-1.107,2.467v1.441a3.154,3.154,0,0,0,1.107,2.468,4.218,4.218,0,0,0,2.885.979,4.461,4.461,0,0,0,2.988-.979,3.108,3.108,0,0,0,1.154-2.468Zm-15.447-3.334h-1.371v6.246l-.041.013-4.88-6.259h-1.37v8.106h1.37v-6.258l.042-.011,4.878,6.269h1.371Zm-15.439,0H1328.5v3.662h-4.921v-3.662h-1.37v8.106h1.37v-3.581h4.921v3.581h1.371Zm-16.116,5.506a1.639,1.639,0,0,1-.627,1.362,2.8,2.8,0,0,1-1.766.492,2.364,2.364,0,0,1-1.831-.74,2.62,2.62,0,0,1-.682-1.847v-1.451a2.611,2.611,0,0,1,.682-1.836,2.373,2.373,0,0,1,1.831-.738,2.779,2.779,0,0,1,1.766.5,1.628,1.628,0,0,1,.627,1.347h1.315l.012-.033a2.209,2.209,0,0,0-.987-1.942,4.588,4.588,0,0,0-2.734-.731,4.027,4.027,0,0,0-2.8.976,3.191,3.191,0,0,0-1.082,2.475v1.441a3.185,3.185,0,0,0,1.082,2.473,4.028,4.028,0,0,0,2.8.974,4.437,4.437,0,0,0,2.718-.77,2.232,2.232,0,0,0,1-1.914l-.012-.033Zm-13.436-2.019h-4.64V164.87h5.272v-.862H1294.3v8.106h6.722v-.863h-5.35v-2.895h4.64Zm-12.774-3.486h-7.878v.862h3.249v7.244h1.371V164.87h3.258Zm-23.268,3.486h-4.642V164.87h5.276v-.862h-6.646v8.106h6.724v-.863h-5.354v-2.895h4.642Zm-19.024-2.624h1.936a2.779,2.779,0,0,1,1.491.327,1.169,1.169,0,0,1,.038,1.918,2.113,2.113,0,0,1-1.244.338h-2.221Zm2.576,3.446a1.909,1.909,0,0,1,1.339.4,1.454,1.454,0,0,1,.437,1.115,1.208,1.208,0,0,1-.523,1.045,2.486,2.486,0,0,1-1.453.37h-2.376v-2.935Zm-.2,3.8a4.432,4.432,0,0,0,2.443-.587,1.9,1.9,0,0,0,.9-1.7,1.856,1.856,0,0,0-.526-1.308A2.469,2.469,0,0,0,1249,167.8a2.663,2.663,0,0,0,1.135-.631,1.323,1.323,0,0,0,.43-.967,1.806,1.806,0,0,0-.9-1.645,4.767,4.767,0,0,0-2.484-.548h-3.306v8.106Zm-12.632-8.106v5.49a1.576,1.576,0,0,1-.693,1.393,3.139,3.139,0,0,1-1.818.477,2.866,2.866,0,0,1-1.726-.477,1.613,1.613,0,0,1-.654-1.393v-5.49h-1.371v5.5a2.3,2.3,0,0,0,1.041,2.024,4.75,4.75,0,0,0,2.711.7,5.067,5.067,0,0,0,2.808-.7,2.275,2.275,0,0,0,1.074-2.027v-5.5Zm-12.794,0h-7.878v.862h3.25v7.244h1.371V164.87h3.257Z" transform="translate(-1127.622 -122.053)"/>
		</svg>';
		$context['white_logo'] = '<svg class="logo negative xclss" xmlns="http://www.w3.org/2000/svg" width="292" height="50.177" viewBox="0 0 292 50.177">
			<defs>
				<style>.a{fill:#0090df;}.b{fill:#464545;}</style>
			</defs>
			<path class="a" d="M1071.6,82.982a24.945,24.945,0,1,0,0,49.89h31.484v-31.6h-30.93a6.586,6.586,0,1,0,0,13.171h16a2.2,2.2,0,1,1,0,4.392h-16a10.978,10.978,0,0,1,0-21.956h35.321v35.992h13.836V82.982Z" transform="translate(-1046.656 -82.982)"/><path class="b" d="M1330.706,104.137a4.65,4.65,0,0,1-1.156,3.382,4.916,4.916,0,0,1-3.624,1.177h-5.318a4.967,4.967,0,0,1-3.688-1.177,4.705,4.705,0,0,1-1.135-3.382V84.011h-5.6v20.524c0,6.415,3.3,9.667,9.8,9.667h6.294c3.242,0,5.72-.818,7.365-2.433s2.479-4.049,2.479-7.234V84.011h-5.419Z" transform="translate(-1173.916 -83.479)"/><path class="b" d="M1379.036,108.3a4.833,4.833,0,0,1-1.831-1.054,3.636,3.636,0,0,1-.961-1.58,6.824,6.824,0,0,1-.282-1.976V84.011h-5.507v19.726a13.82,13.82,0,0,0,.6,4.186,8.329,8.329,0,0,0,1.96,3.312,9.2,9.2,0,0,0,3.485,2.184,14.921,14.921,0,0,0,5.123.784h12.06V108.7h-11.75A9.2,9.2,0,0,1,1379.036,108.3Z" transform="translate(-1203.018 -83.479)"/><rect class="b" width="5.508" height="30.191" transform="translate(195.897 0.532)"/><path class="b" d="M1469.315,99.574a8.356,8.356,0,0,0,1.691-3.064,11.8,11.8,0,0,0,.515-3.408,10.757,10.757,0,0,0-.562-3.457,7.727,7.727,0,0,0-1.759-2.91,8.658,8.658,0,0,0-3.04-1.983,11.812,11.812,0,0,0-4.4-.741h-15.427v5.507h15.65a3.766,3.766,0,0,1,3.018,1.125,4.086,4.086,0,0,1,.964,2.724,4.722,4.722,0,0,1-.236,1.485,3.8,3.8,0,0,1-.706,1.262,3.405,3.405,0,0,1-1.235.894,4.565,4.565,0,0,1-1.849.342h-10.77a5.077,5.077,0,0,0-3.687,1.148,5.148,5.148,0,0,0-1.148,3.732V114.2h5.506V103.25c0-.425.19-.615.615-.615h9.307a10.83,10.83,0,0,0,4.512-.855A8.8,8.8,0,0,0,1469.315,99.574Z" transform="translate(-1239.662 -83.479)"/><path class="b" d="M1509.411,83.864a7.986,7.986,0,0,0-3.946-.882,8.092,8.092,0,0,0-3.991.882,5.806,5.806,0,0,0-2.432,3.061l-10.576,26.78h6.06l9.515-24.148a1.4,1.4,0,0,1,1.38-.935,1.318,1.318,0,0,1,1.331.929l9.085,24.064.034.09h6.1L1511.8,86.928A5.609,5.609,0,0,0,1509.411,83.864Z" transform="translate(-1260.005 -82.982)"/><path class="b" d="M1584.123,99.168a8.34,8.34,0,0,0-3.133-2.052,13.905,13.905,0,0,0-5.058-.786h-7a6.714,6.714,0,0,1-2.127-.28,3.325,3.325,0,0,1-1.3-.764,2.663,2.663,0,0,1-.676-1.1,4.3,4.3,0,0,1-.191-1.266,3.467,3.467,0,0,1,.892-2.453,3.779,3.779,0,0,1,2.867-.953h15.473V84.011h-15.295a12.736,12.736,0,0,0-4.3.651,7.834,7.834,0,0,0-2.955,1.828,7,7,0,0,0-1.67,2.78,11.259,11.259,0,0,0-.517,3.476,11.966,11.966,0,0,0,.426,3.138,7.07,7.07,0,0,0,1.535,2.847,7.753,7.753,0,0,0,3.044,2.033,13.762,13.762,0,0,0,4.973.763h6.957a7.068,7.068,0,0,1,2.237.3,3.753,3.753,0,0,1,1.389.789,2.707,2.707,0,0,1,.717,1.139,4.412,4.412,0,0,1,.215,1.353,3.6,3.6,0,0,1-.958,2.585,4.041,4.041,0,0,1-3.025,1h-16.979V114.2h16.8a12.845,12.845,0,0,0,4.415-.673,8.069,8.069,0,0,0,3-1.872,7.27,7.27,0,0,0,1.716-2.823,11.17,11.17,0,0,0,.539-3.546,12.634,12.634,0,0,0-.425-3.226A6.847,6.847,0,0,0,1584.123,99.168Z" transform="translate(-1294.13 -83.479)"/><path class="b" d="M1228.684,101.716h12.61V103.6h0v.7a4.671,4.671,0,0,1-1.216,3.382,4.909,4.909,0,0,1-3.622,1.177h-8.851a10.269,10.269,0,0,1-6.857-2.942c-1.73-1.73-2.3-4.867-2.3-7.315,0-5.888,4.109-9.146,9.16-9.146h19.048V84.081h-19.42a15.019,15.019,0,1,0,0,30.039h5.908v-.005h3.668c3.242,0,5.721-.818,7.365-2.433s2.479-4.049,2.479-7.234V96.394h-17.97Z" transform="translate(-1126.603 -83.513)"/><path class="b" d="M1289.678,84.081h-.755V114.12h5.769V84.081Z" transform="translate(-1163.647 -83.513)"/><path class="a" d="M1411.3,164.008h-1.565l3.5,5.182v2.924h1.364v-2.851l3.557-5.255h-1.566l-2.644,4.12Zm-8.1,4.058h-3.589v.863h2.219v1.839a2.062,2.062,0,0,1-.785.417,4.639,4.639,0,0,1-1.414.183,2.951,2.951,0,0,1-2.02-.7,2.262,2.262,0,0,1-.791-1.776v-1.681a2.363,2.363,0,0,1,.711-1.764,2.557,2.557,0,0,1,1.872-.7,2.882,2.882,0,0,1,1.754.456,1.587,1.587,0,0,1,.673,1.165h1.309l.012-.034a2.108,2.108,0,0,0-1.014-1.764,4.756,4.756,0,0,0-2.734-.685,4.3,4.3,0,0,0-2.855.929,3,3,0,0,0-1.107,2.406v1.67a2.91,2.91,0,0,0,1.188,2.4,4.71,4.71,0,0,0,3,.931,6.064,6.064,0,0,0,2.314-.373,3.181,3.181,0,0,0,1.255-.819Zm-16.031.717a2.422,2.422,0,0,1-.747,1.84,2.816,2.816,0,0,1-2.023.711,2.576,2.576,0,0,1-1.908-.712,2.481,2.481,0,0,1-.715-1.839v-1.453a2.471,2.471,0,0,1,.715-1.826,2.575,2.575,0,0,1,1.908-.711,2.822,2.822,0,0,1,2.02.711,2.407,2.407,0,0,1,.75,1.826Zm1.371-1.441a3.111,3.111,0,0,0-1.15-2.469,4.445,4.445,0,0,0-2.991-.982,4.186,4.186,0,0,0-2.883.985,3.152,3.152,0,0,0-1.11,2.467v1.441a3.147,3.147,0,0,0,1.11,2.468,4.208,4.208,0,0,0,2.883.979,4.47,4.47,0,0,0,2.991-.979,3.11,3.11,0,0,0,1.15-2.468Zm-19.095-3.334h-1.369v8.106h6.165v-.863h-4.8Zm-10.059,4.775a2.424,2.424,0,0,1-.748,1.84,2.816,2.816,0,0,1-2.023.711,2.582,2.582,0,0,1-1.91-.712,2.48,2.48,0,0,1-.713-1.839v-1.453a2.47,2.47,0,0,1,.713-1.826,2.581,2.581,0,0,1,1.91-.711,2.816,2.816,0,0,1,2.019.711,2.4,2.4,0,0,1,.752,1.826Zm1.371-1.441a3.109,3.109,0,0,0-1.154-2.469,4.437,4.437,0,0,0-2.988-.982,4.2,4.2,0,0,0-2.885.985,3.159,3.159,0,0,0-1.107,2.467v1.441a3.154,3.154,0,0,0,1.107,2.468,4.218,4.218,0,0,0,2.885.979,4.461,4.461,0,0,0,2.988-.979,3.108,3.108,0,0,0,1.154-2.468Zm-15.447-3.334h-1.371v6.246l-.041.013-4.88-6.259h-1.37v8.106h1.37v-6.258l.042-.011,4.878,6.269h1.371Zm-15.439,0H1328.5v3.662h-4.921v-3.662h-1.37v8.106h1.37v-3.581h4.921v3.581h1.371Zm-16.116,5.506a1.639,1.639,0,0,1-.627,1.362,2.8,2.8,0,0,1-1.766.492,2.364,2.364,0,0,1-1.831-.74,2.62,2.62,0,0,1-.682-1.847v-1.451a2.611,2.611,0,0,1,.682-1.836,2.373,2.373,0,0,1,1.831-.738,2.779,2.779,0,0,1,1.766.5,1.628,1.628,0,0,1,.627,1.347h1.315l.012-.033a2.209,2.209,0,0,0-.987-1.942,4.588,4.588,0,0,0-2.734-.731,4.027,4.027,0,0,0-2.8.976,3.191,3.191,0,0,0-1.082,2.475v1.441a3.185,3.185,0,0,0,1.082,2.473,4.028,4.028,0,0,0,2.8.974,4.437,4.437,0,0,0,2.718-.77,2.232,2.232,0,0,0,1-1.914l-.012-.033Zm-13.436-2.019h-4.64V164.87h5.272v-.862H1294.3v8.106h6.722v-.863h-5.35v-2.895h4.64Zm-12.774-3.486h-7.878v.862h3.249v7.244h1.371V164.87h3.258Zm-23.268,3.486h-4.642V164.87h5.276v-.862h-6.646v8.106h6.724v-.863h-5.354v-2.895h4.642Zm-19.024-2.624h1.936a2.779,2.779,0,0,1,1.491.327,1.169,1.169,0,0,1,.038,1.918,2.113,2.113,0,0,1-1.244.338h-2.221Zm2.576,3.446a1.909,1.909,0,0,1,1.339.4,1.454,1.454,0,0,1,.437,1.115,1.208,1.208,0,0,1-.523,1.045,2.486,2.486,0,0,1-1.453.37h-2.376v-2.935Zm-.2,3.8a4.432,4.432,0,0,0,2.443-.587,1.9,1.9,0,0,0,.9-1.7,1.856,1.856,0,0,0-.526-1.308A2.469,2.469,0,0,0,1249,167.8a2.663,2.663,0,0,0,1.135-.631,1.323,1.323,0,0,0,.43-.967,1.806,1.806,0,0,0-.9-1.645,4.767,4.767,0,0,0-2.484-.548h-3.306v8.106Zm-12.632-8.106v5.49a1.576,1.576,0,0,1-.693,1.393,3.139,3.139,0,0,1-1.818.477,2.866,2.866,0,0,1-1.726-.477,1.613,1.613,0,0,1-.654-1.393v-5.49h-1.371v5.5a2.3,2.3,0,0,0,1.041,2.024,4.75,4.75,0,0,0,2.711.7,5.067,5.067,0,0,0,2.808-.7,2.275,2.275,0,0,0,1.074-2.027v-5.5Zm-12.794,0h-7.878v.862h3.25v7.244h1.371V164.87h3.257Z" transform="translate(-1127.622 -122.053)"/>
		</svg>';
		$context['status_bar'] = get_option( 'statusbar_color' );


		/* Social options */
		$social_networks = array();

		if ( get_option( 'twitter_url' ) ) {
			$social_networks[] = array(
				'title' => 'Twitter',
				'link'  => get_option( 'twitter_url' )
			);
		}

		if ( get_option( 'facebook_url' ) ) {
			$social_networks[] = array(
				'title' => 'Facebook',
				'link'  => get_option( 'facebook_url' )
			);
		}

		if ( get_option( 'googleplus_url' ) ) {
			$social_networks[] = array(
				'title' => 'Google+',
				'link'  => get_option( 'googleplus_url' )
			);
		}

		if ( get_option( 'instagram_url' ) ) {
			$social_networks[] = array(
				'title' => 'Instagram',
				'link'  => get_option( 'instagram_url' )
			);
		}

		if ( get_option( 'youtube_url' ) ) {
			$social_networks[] = array(
				'title' => 'YouTube',
				'link'  => get_option( 'youtube_url' )
			);
		}

		$context['social_networks'] = $social_networks;


		/* Google options */
		$google_fonts = array(
			get_option( 'googlefont_url' ),
			get_option( 'googlefont_2_url' ),
			get_option( 'googlefont_3_url' )
		);

		if ( !empty( $google_fonts ) ) {
			$context['fonts'] = array_filter($google_fonts, function( $s ) {
				return !is_null( $s ) && $s !== '';
			} );
		}

		$context['map_api_key'] = MAP_API_KEY;


		/* General contact */
		$context['contacts'] = array(
			'ragione_sociale' => get_option( 'contact_ragione_sociale' ),
			'indirizzo'       => get_option( 'contact_indirizzo' ),
			'telefono'        => get_option( 'contact_telefono' ),
			'fax'             => get_option( 'contact_fax' ),
			'email'           => get_option( 'contact_mail' )
		);


		/* Footer options */
		$context['footer_text']  = get_option( 'footer_text' );
		$context['privacy_link'] = get_the_privacy_policy_link();

		if ( get_option( 'page_for_credits' ) ) {
			$link       = '';
			$page_id    = get_option( 'page_for_credits' );
			$page_url   = get_permalink( $page_id );
			$page_title = ( $page_id ) ? get_the_title( $page_id ) : '';

			if ( $page_url && $page_title ) {
				$link = sprintf(
					'<a href="%s">%s</a>',
					esc_url( $page_url ),
					esc_html( $page_title )
				);
			}

			$context['page_for_credits'] = $link;
		}


		/* Sidebars */
		$context['sidebars'] = array();

		global $wp_registered_sidebars;

		foreach ( $wp_registered_sidebars as $id => $array ) {
			if ( is_active_sidebar( $id ) ) {
				$context[ $id ] = Timber::get_widgets( $id );
			}
		}
		
		
		$context['is_front_page']     = is_front_page();
		$context['breadcrumb']        = yoast_breadcrumb('<div id="breadcrumb" class="container-fluid">','</div>', false );
		$context['blog_link']         = get_post_type_archive_link( 'post' );
		$context['case_history_link'] = get_post_type_archive_link( 'case-history' );

        $context['theme_textdomain']  = MST_THEME_DOMAIN;
        
		return $context;
	}


	/**
	 * Adds custom functions to Twig.
	 *
	 * @param {string} $twig : get extension.
	 */
	function add_to_twig( $twig ) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );

		// Adding a function.
		//$twig->addFunction( new Timber\Twig_Function( 'my_function', 'my_function' ) );



		// Adding functions as filters.
		$twig->addFilter( new Timber\Twig_Filter( 'leading_zero', function( $number, $length = 2 ) {
			return str_pad( $number, $length, '0', STR_PAD_LEFT );
		} ) );

		$twig->addFilter( new Timber\Twig_Filter( 'mail_to', function( $string, $class = '' ) {
			if ( preg_match_all( '/[\p{L}0-9_.-]+@[0-9\p{L}.-]+\.[a-z.]{2,6}\b/u', $string, $mails ) ) {
				foreach ( $mails[0] as $mail ) {
					$string = str_replace( $mail, '<a class="' . $class . '" href="mailto:' . $mail . '">' . $mail . '</a>', $string );
				}
			}

			return $string;
		} ) );

		return $twig;
	}


	function register_post_types() {
		require('inc/custom-types.php');
	}


	function register_taxonomies() {
		require('inc/taxonomies.php');
	}


	function register_widgets() {
		require('inc/widgets.php');
	}


	function register_blocks() {
		require('inc/blocks.php');
	}


	function register_block_patterns() {
		require('inc/block-patterns.php');
	}
}

new GiulipasSite();



/**
 * Definisce la funzione per scrivere nel file di log.
 *
 * @since 1.0.0
 */
if ( !function_exists( '_log' ) ) {
	function _log( $message ) {
		if ( WP_DEBUG === true ) {
			if ( is_array( $message ) || is_object( $message ) ) {
				error_log( print_r( $message, true ) );
			} else {
				error_log( $message );
			}
		} else {
			if ( is_array( $message ) || is_object( $message ) ) {
				$message = json_encode( $message );
			}

			$file = fopen( WP_CONTENT_DIR . '/debug.log', 'a' );

			fwrite( $file, "\n" . '[' . date('d-M-Y H:i:s') . '] ' . $message );
			fclose( $file );
		}
	}
}



/**
 * Personalizza la pagina 404 con una pagina vera e propria.
 *
 * @since 1.0.0
 */
Routes::map('/:page', function ($params) {
	$post_types = get_post_types( array( 'public' => true ), 'names', 'and' );
	$page       = get_page_by_path( $params['page'], OBJECT, $post_types );
	
	if ( $page ) {
		if ( 'page' == $page->post_type ) {
			Routes::load( 'routes/page.php', $params, null, 200 );
		}
	} else {
		$query = 'post_type=page&p=' . get_option( 'page_404' );
		
		Routes::load( '404.php', null, $query, 404 );
	}
});




add_filter(
	'register_post_type_args',
	function ($args, $post_type) {

		if ($post_type !== 'post') {
			return $args;
		}
		if( class_exists( "TimberPost" ) ){
		$news_page = new TimberPost(get_option( 'page_for_posts' ));

		$args['rewrite'] = [
			'slug' =>  $news_page->slug,
			'with_front' => true,
		];
		}

		return $args;
	},
	10,
	2
);

add_filter(
	'pre_post_link',
	function ($permalink, $post) {

		if ($post->post_type !== 'post') {
			return $permalink;
		}

		$news_page = new TimberPost(get_option( 'page_for_posts' ));

		return '/'.$news_page->slug.'/%postname%/';
	},
	10,
	2
);