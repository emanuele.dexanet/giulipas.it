<?php
/**
 * The main template file
 *
 * @since 1.0.0
 */

$templates        = array( 'index.twig' );
$context          = Timber::context();
$context['posts'] = new Timber\PostQuery();

if ( is_front_page() ) {
	array_unshift( $templates, 'front-page.twig', 'home.twig' );
}

if ( is_home() ) {
	$timber_post = new Timber\Post();
	
	if ( $timber_post ) {
		$context['post'] = $timber_post;
	}

	array_unshift( $templates, 'archive-' . get_post_type() . '.twig', 'page-' . $timber_post->post_name . '.twig', 'page.twig' );
}

Timber::render( $templates, $context );
