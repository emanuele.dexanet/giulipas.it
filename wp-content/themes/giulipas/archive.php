<?php
/**
 * The template for displaying Archive pages.
 *
 * @since 1.0.0
 */

$templates        = array( 'archive.twig', 'index.twig' );
$context          = Timber::context();
$context['title'] = 'Archive';

if ( is_day() ) {
	$context['title'] = sprintf( __( 'Archive: %s', MST_THEME_DOMAIN ), get_the_date( 'D M Y' ) );
} elseif ( is_month() ) {
	$context['title'] = sprintf( __( 'Archive: %s', MST_THEME_DOMAIN ), get_the_date( 'M Y' ) );
} elseif ( is_year() ) {
	$context['title'] = sprintf( __( 'Archive: %s', MST_THEME_DOMAIN ), get_the_date( 'Y' ) );
} elseif ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );
} elseif ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	
	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} elseif ( is_post_type_archive() ) {
	if ( 'case-history' == get_post_type() && 0 != get_option( 'case_history_archive_page' ) ) {
		$timber_post = new Timber\Post( get_option( 'case_history_archive_page' ) );
	
		$context['post'] = $timber_post;

		array_unshift( $templates, 'archive-' . get_post_type() . '.twig', 'page-' . $timber_post->post_name . '.twig', 'page.twig' );
	} elseif ( 'post' == get_post_type() && 0 != get_option( 'page_for_posts' ) ) {
		$timber_post = new Timber\Post( get_option( 'page_for_posts' ) );
	
		$context['post'] = $timber_post;
		
		array_unshift( $templates, 'archive-' . get_post_type() . '.twig', 'page-' . $timber_post->post_name . '.twig', 'page.twig' );
	} else {
		$context['title'] = post_type_archive_title( '', false );
		
		array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
	}
}

$context['posts'] = new Timber\PostQuery();

Timber::render( $templates, $context );