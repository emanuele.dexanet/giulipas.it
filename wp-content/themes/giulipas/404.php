<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @since 1.0.0
 */

$context         = Timber::context();
$context['post'] = new Timber\Post();

Timber::render( '404.twig', $context );