<?php
/**
 * The template for displaying all pages.
 *
 * @since 1.0.0
 */

$context         = Timber::context();
$timber_post     = new Timber\Post();
$context['post'] = $timber_post;
$templates       = array( 'page-' . $timber_post->post_name . '.twig', 'page.twig' );

Timber::render( $templates, $context );
