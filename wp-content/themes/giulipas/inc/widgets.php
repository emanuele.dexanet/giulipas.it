<?php
/**
 * Widgets and Sidebars
 *
 */
register_sidebar( 
	array(
		'name'           => 'Header 1',
		'id'             => 'header_top',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);

register_sidebar( 
	array(
		'name'           => 'Header 2',
		'id'             => 'header_top_2',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);

register_sidebar( 
	array(
		'name'           => 'Header 3',
		'id'             => 'header_top_3',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);

register_sidebar( 
	array(
		'name'           => 'Header 4',
		'id'             => 'header_top_4',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);



/**
 * Rimuove le sidebar registrate del tema padre per poter modificare i parametri "before_title" e "after_title".
 *
 * @since 1.0.0
 */
unregister_sidebar( 'footer_bottom' );
unregister_sidebar( 'footer_bottom_2' );
unregister_sidebar( 'footer_bottom_3' );
unregister_sidebar( 'footer_bottom_4' );

register_sidebar( 
	array(
		'name'           => 'Footer 1',
		'id'             => 'footer_bottom',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);

register_sidebar( 
	array(
		'name'           => 'Footer 2',
		'id'             => 'footer_bottom_2',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);

register_sidebar( 
	array(
		'name'           => 'Footer 3',
		'id'             => 'footer_bottom_3',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);

register_sidebar( 
	array(
		'name'           => 'Footer 4',
		'id'             => 'footer_bottom_4',
		'description'    => '',
        'class'          => '',
		'before_widget'  => '<div class="widget xclass %2$s">',
		'after_widget'   => '</div>',
		'before_title'   => '<h3 class="widget-title">',
		'after_title'    => '</h3>',
		'before_sidebar' => '',
        'after_sidebar'  => '',
	) 
);