<?php
/**
 * Rimuove la gestione dei template utilizzata dal tema padre.
 *
 * @since 1.0.0
 */
if ( class_exists( 'Mad_Wrapping' ) ) {
	remove_filter( 'template_include', array( 'Mad_Wrapping', 'wrap' ), 99, 1 );
}



/**
 * Nasconde la barra di amministrazione per tutti gli utenti.
 *
 * @since 1.0.0
 */
/*
function mst_hide_admin_bar() {
    return false;
}

add_filter( 'show_admin_bar', 'mst_hide_admin_bar' );
*/



/**
 * Permette di utilizzare gli shortcode anche all'interno del widget "Testo".
 *
 * @since 1.0.0
 */
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');



/**
 * Permette di utilizzare gli shortcode anche all'interno delle etichette delle voci di menu.
 *
 * @since 1.0.0
 */
add_filter( 'wp_nav_menu_items', 'do_shortcode' );



/**
 * Add "async" and "defer" attributes to Google Maps script tag.
 *
 * @since 1.0.0
 */
function mst_add_async_defer_attribute( $tag, $handle ) {
	if ( 'googlemaps_api' !== $handle ) {
		return $tag;
	}

	return str_replace( ' src', ' async defer src', $tag );
}

add_filter( 'script_loader_tag', 'mst_add_async_defer_attribute', 10, 2) ;



/**
 * Personalizza i parametri dei widget.
 *
 * @since 1.0.0
 */
function mst_dynamic_sidebar_params( $params ) {
	// $params è un array del tipo:
	//    Array
	//    (
	//       name
	//       id
	//       description
	//       class
	//       before_widget
	//       after_widget
	//       before_title
	//       after_title
	//       before_sidebar
	//       after_sidebar
	//       widget_id
	//       widget_name
	//    )
	$widget_name       = $params[0]['widget_name'];
	$widget_id         = $params[0]['widget_id'];
	$hide_widget_title = get_field( 'wpw_show_widget_title', 'widget_' . $widget_id );
	$widget_class      = get_field( 'wpw_css_classes', 'widget_' . $widget_id );
	
	if ( $hide_widget_title ) {
		$params[0]['before_title'] = str_replace( 'widget-title', 'widget-title d-none', $params[0]['before_title'] );
	}
	
	$params[0]['before_widget'] = str_replace( 'xclass', $widget_class, $params[0]['before_widget'] );
	
	return $params;
}

add_filter( 'dynamic_sidebar_params', 'mst_dynamic_sidebar_params' );



/**
 * Add custom state to post/page list.
 *
 * @since 1.0.0
 */
function mst_add_custom_post_states( $states ) {
	global $post;

	$case_history_archive_page_id = get_option( 'case_history_archive_page' );

	if ( is_object( $post ) && 'page' == get_post_type( $post->ID ) && $post->ID == $case_history_archive_page_id && $case_history_archive_page_id != '0' ) {
		$states[] = __( '– Case Histories Page', MST_THEME_DOMAIN );
	}

	return $states;
}

add_filter( 'display_post_states', 'mst_add_custom_post_states' );
