<?php
/**
 * Aggiunge una scheda all'editor presente nella pagina di modifica di una form.
 * Solo per versioni di Contact Form 7 >= 4.2
 *
 * @param  {array} $panels : 
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_wpcf7_add_redirect_panels( $panels ) {
	$panels['redirect-panel'] = array(
		'title'    => __( 'Redirect settings', MST_THEME_DOMAIN ),
		'callback' => 'mst_wpcf7_redirect_panel_form',
	);
	
	return $panels;
}

add_filter( 'wpcf7_editor_panels', 'mst_wpcf7_add_redirect_panels', 10, 1 );



/**
 * Render callback della nuova scheda.
 *
 * @param {WPCF7_ContactForm} $contact_form : 
 *
 * @since 1.0.0
 */
function mst_wpcf7_redirect_panel_form( $contact_form ) {
	$value = get_post_meta( $contact_form->id(), 'mst_wpcf7_redirect', true );
	?>

	<h2><?php _e( 'Redirect page', MST_THEME_DOMAIN ); ?></h2>
	<fieldset>
		<legend><?php _e( 'Select a page to redirect to when a form submission completes and mail has been sent successfully.', MST_THEME_DOMAIN ); ?></legend>
		<?php wp_dropdown_pages( array(
			'echo'              => 1,
			'name'              => 'wpcf7-redirect',
			'id'                => 'wpcf7-redirect',
			'selected'          => $value,
			'show_option_none' 	=> '— Seleziona —', 
			'option_none_value' => '0',
			'value_field'       => 'ID'
		) ); ?>
	</fieldset>

<?php
}



/**
 * Salva il valore dei campi presenti nella nuova scheda.
 *
 * @param {WPCF7_ContactForm} $contact_form : 
 *
 * @since 1.0.0
 */
function mst_wpcf7_redirect_save_field( $contact_form ) {
	if ( 0 != $_POST['wpcf7-redirect'] ) {
		update_post_meta( $contact_form->id(), 'mst_wpcf7_redirect', $_POST['wpcf7-redirect'] );
	} else {
		delete_post_meta( $contact_form->id(), 'mst_wpcf7_redirect' );
	}
}

add_action( 'wpcf7_after_save', 'mst_wpcf7_redirect_save_field' );



/**
 * Registra i dati sui moduli.
 *
 * @param  {array} $out : 
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_shortcode_atts_wpcf7( $out ) {
	mst_wpcf7_redirect_add_form_data( $out['id'] );

	return $out;
}



/**
 * Restituisce i dati sui moduli.
 *
 * @param  {int|null} $form_id : 
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_wpcf7_redirect_add_form_data( $form_id = null ) {
	static $forms = array();

	if ( $form_id && empty( $forms[ $form_id ] ) ) {
		$forms[ $form_id ] = get_permalink( get_post_meta( $form_id, 'mst_wpcf7_redirect', true ) );
	}

	return $forms;
}

add_filter( 'shortcode_atts_wpcf7', 'mst_shortcode_atts_wpcf7' );



/**
 * Include gli script e gli stili.
 *
 * @return {void}
 *
 * @since 1.0.0
 */
function mst_wpcf7_redirect_enqueue_assets() {
	$forms   = array_filter( mst_wpcf7_redirect_add_form_data() );
	$scripts = '';
	$styles  = '';

	foreach ( $forms as $id => $link ) {
		$scripts .= "if ( $id == event.detail.contactFormId ) { window.location.href = '$link'; }";
		$styles  .= ".wpcf7[id*='f$id'] form.sent .wpcf7-response-output { display: none !important; }";
	}
	
	if ( $scripts ) {
		?>

		<script>
			document.addEventListener( 'wpcf7mailsent', function( event ) {
				<?php echo $scripts; ?>
			}, false );
		</script>

		<style>
			<?php //echo $styles; ?>
		</style>

		<?php
    }
}

add_action( 'wp_footer', 'mst_wpcf7_redirect_enqueue_assets' );