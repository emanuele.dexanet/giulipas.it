<?php
/**
 * Aggiunge i fogli di stile e gli script alle pagine (lato backend) in cui sono presenti campi ACF.
 *
 * @since 1.0.0
 */
function mst_acf_admin_enqueue_scripts() {
	global $post;
	global $pagenow;
	
	if ( $post && isset( $post->ID ) ) {
		$vars = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'sliders' => json_encode( mst_acf_sliders_data_object() ),
		);
		
		wp_enqueue_script( 
			'acf_block_slideshow_script', 
			MST_THEME_URL . 'assets/js/acf-block-slideshow.js', 
			array( 'acf-input', 'jquery' ) 
		);
		wp_localize_script( 
			'acf_block_slideshow_script', 
			'mst_ajax_vars', 
			$vars 
		);
	}
}

//add_action( 'acf/input/admin_enqueue_scripts', 'mst_acf_admin_enqueue_scripts' );