<?php
/**
 * Popola dinamicamente i radio button con i colori della palette definita per il tema.
 *
 * @since 1.0.0
 */
function mst_acf_dynamic_colors_load( $field ) {
	$colors = get_theme_support( 'editor-color-palette' );

	if ( !empty( $colors ) ) {
		$field['choices'] = array();
		
		foreach( $colors[0] as $color ) {
			$field['choices'][ $color['color'] ] = $color['slug'];//sprintf( '<a href="javascript:void(0)" title="%1$s">%2$s</a>', $color['name'], $color['name'] );
		}
	}

	return $field;
}

add_filter( 'acf/load_field/name=wpb_text_color', 'mst_acf_dynamic_colors_load' );
add_filter( 'acf/load_field/name=wpb_background_color', 'mst_acf_dynamic_colors_load' );
add_filter( 'acf/load_field/name=wpb_color', 'mst_acf_dynamic_colors_load' );



/**
 * Popola dinamicamente la select per la scelta della griglia.
 *
 * @since 1.0.0
 */
function mst_acf_dynamic_grid_layout( $field ) {
	$field['choices'] = array();
	
	switch ( $field['name'] ) {
		case 'wpb_grid_small':
			$prefix = 'col-';
			break;
		case 'wpb_grid_medium':
			$prefix = 'col-md-';
			break;
		case 'wpb_grid_large':
			$prefix = 'col-lg-';
			break;
		case 'wpb_grid_extra_large':
			$prefix = 'col-xl-';
			break;
		default:
			$prefix = 'col-';
	}
	
	$values = array(
		$prefix . '1'  => '1/12',
		$prefix . '2'  => '2/12',
		$prefix . '3'  => '3/12',
		$prefix . '4'  => '4/12',
		$prefix . '5'  => '5/12',
		$prefix . '6'  => '6/12',
		$prefix . '7'  => '7/12',
		$prefix . '8'  => '8/12',
		$prefix . '9'  => '9/12',
		$prefix . '10'  => '10/12',
		$prefix . '11'  => '11/12',
		$prefix . '12'  => '12/12',
	);

	foreach( $values as $key => $label ) {
		$field['choices'][ $key ] = $label;
	}

	return $field;
}

add_filter( 'acf/load_field/name=wpb_grid_small', 'mst_acf_dynamic_grid_layout' );
add_filter( 'acf/load_field/name=wpb_grid_medium', 'mst_acf_dynamic_grid_layout' );
add_filter( 'acf/load_field/name=wpb_grid_large', 'mst_acf_dynamic_grid_layout' );
add_filter( 'acf/load_field/name=wpb_grid_extra_large', 'mst_acf_dynamic_grid_layout' );



/**
 * Popola dinamicamente la select per la scelta dei blocchi riutilizzabili.
 *
 * @since 1.0.0
 */
function mst_acf_reusable_blocks( $field ) {
	$field['choices'] = array();
	$args             = array(
		'numberposts' => -1,
		'orderby'     => 'title',
		'order'       => 'ASC',
		'post_type'   => 'wp_block',
		'post_status' => 'publish'
	);
	$reusable_blocks  = get_posts( $args);
	
	if ( $reusable_blocks ) {
		foreach ( $reusable_blocks as $block ) {
			$field['choices'][ $block->ID ] = $block->post_title;
		}
	}
	
	return $field;
}

add_filter( 'acf/load_field/name=wppt_reusable_blocks', 'mst_acf_reusable_blocks' );



/**
 * Popola dinamicamente la select per la scelta delle slide.
 *
 * @since 1.0.0
 */
function mst_acf_dynamic_slides( $field ) {
	$field['choices'] = array();
	$args             = array(
		'posts_per_page' => -1,
		'orderby'        => 'title',
		'order'          => 'ASC',
		'post_type'      => 'slide',
		'post_status'    => 'publish',
	);
	$slides          = get_posts( $args );
	
	if ( $slides ) {
		foreach ( $slides as $slide ) {
			$field['choices'][ $slide->ID ] = $slide->post_title;
		}
	}

	return $field;
}

add_filter( 'acf/load_field/name=wpb_slideshow_slides', 'mst_acf_dynamic_slides' );




/**
 * .
 *
 * @since 1.0.0
 */
/*
function mst_acf_icon_field_label( $field ) {
	$field['label'] = '';

	return $field;
}

add_filter( 'acf/load_field/name=wpb_counters_columns_small', 'mst_acf_icon_field_label' );
*/