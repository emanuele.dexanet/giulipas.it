<?php
/**
 * Richiama le funzioni per le chiamate AJAX.
 *
 * $action è il valore della proprietà "action" della chiamta AJAX
 * $callback è il nome della funzione di callback associata
 *
 * add_action( 'wp_ajax_$action', '$callback' )
 * add_action( 'wp_ajax_nopriv_$action', '$callback' )
 *
 * @since 1.0.0
 */
add_action( 'wp_ajax_load_more_posts', 'mst_load_more_posts' );
add_action( 'wp_ajax_nopriv_load_more_posts', 'mst_load_more_posts' );

add_action( 'wp_ajax_load_more_ch_posts', 'mst_load_more_ch_posts' );
add_action( 'wp_ajax_nopriv_load_more_ch_posts', 'mst_load_more_ch_posts' );


/**
 * Carica nuovi post.
 *
 * @since 1.0.0
 */
function mst_load_more_posts() {
	check_ajax_referer( 'loadmore-' . $_POST['block_id'], '_nonce' );
	
	$page          = esc_attr( $_POST['page'] );
	$rule          = ( isset( $_POST['rule'] ) && !empty( $_POST['rule'] ) ) ? $_POST['rule'] : 'numberposts';
	$per_page      = ( isset( $_POST['per_page'] ) && !empty( $_POST['per_page'] ) ) ? $_POST['per_page'] : get_option( 'posts_per_page' );
	// $blog_posts è un array del tipo:
	//    Array
	//    (
	//       posts
	//       show_loadmore
	//    )
	$blog_posts    = mst_get_blog_posts( $rule, $per_page, $page );
	$posts         = $blog_posts['posts'];
	$show_loadmore = $blog_posts['show_loadmore'];
	$groups        = mst_group_posts( $posts );
	$data          = array();
	$message       = '';
	
	if ( !empty( $groups ) ) {
		foreach ( $groups as $year_month => $group ) {
			$data[ $year_month ] = Timber::compile( 
				'blocks/partials/blog-archive-child-row.twig', 
				array(
					'year_month' => $year_month,
					'group'      => $group
				)
			);
		}
	} else {
		$message = __( 'No posts found.', MST_THEME_DOMAIN );
	}
		
	
	wp_send_json( array(
		'data'          => $data,
		'show_loadmore' => $show_loadmore,
		'message'       => $message
	) );
}



/**
 * Carica nuovi custom post type "case-history".
 *
 * @since 1.0.0
 */
function mst_load_more_ch_posts() {
	check_ajax_referer( 'loadmore-' . $_POST['block_id'], '_nonce' );
	
	$page           = esc_attr( $_POST['page'] );
	$posts_per_page = ( isset( $_POST['posts_per_page'] ) && !empty( $_POST['posts_per_page'] ) ) ? $_POST['posts_per_page'] : get_option( 'posts_per_page' );
	$term_id        = ( isset( $_POST['term_id'] ) && !empty( $_POST['term_id'] ) ) ? $_POST['term_id'] : 0;
	// $ch_posts è un array del tipo:
	//    Array
	//    (
	//       posts
	//       show_loadmore
	//    )
	$ch_posts      = mst_get_case_history_posts( $posts_per_page, $term_id, $page );
	$posts         = $ch_posts['posts'];
	$show_loadmore = $ch_posts['show_loadmore'];
	$data          = array();
	$message       = '';
	
	if ( !empty( $posts ) ) {
		foreach ( $posts as $post ) {
			$data[] = Timber::compile( 
				'blocks/partials/case-history-archive-child-col.twig', 
				array(
					'post' => $post
				)
			);
		}
	} else {
		$message = __( 'No posts found.', MST_THEME_DOMAIN );
	}
	
	wp_send_json( array(
		'data'          => $data,
		'show_loadmore' => $show_loadmore,
		'message'       => $message
	) );
}
