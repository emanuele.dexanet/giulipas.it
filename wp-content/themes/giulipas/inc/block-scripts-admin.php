<?php
/**
 * Gutenberg (back-end).
 *
 * @since 1.0.0
 */
function mst_enqueue_block_editor_assets() {
	/* Styles */
	/*wp_enqueue_style( 
		'unique_stylesheet_name', 
		MST_THEME_URL . 'assets/css/stylesheet.css', 
		false(for no dependencies)|array(), 
		filemtime( MST_THEME_DIR . 'assets/css/stylesheet.css' ), 
		'media|all' 
	);*/
	
	wp_enqueue_style(
		'block_style',
		MST_THEME_URL . 'assets/css/blocks.css',
		false,
		filemtime( MST_THEME_DIR . 'assets/css/blocks.css' ), 
		'all'
	);
	
	
	
	/* Scripts */
	/*wp_enqueue_script( 
		'unique_script_name', 
		MST_THEME_URL . 'assets/js/script.js', 
		false(for no dependencies)|array(), 
		filemtime( MST_THEME_DIR . 'assets/js/script.js' ),
		true(for script before </body>)|false 
	);*/
	
	wp_enqueue_script(
		'mst-editor-script', 
		MST_THEME_URL . 'assets/js/editor.js', 
		array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ), 
		filemtime( MST_THEME_DIR . 'assets/js/editor.js' ),
		true
	);
}

add_action( 'enqueue_block_editor_assets', 'mst_enqueue_block_editor_assets' );