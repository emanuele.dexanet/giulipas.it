<?php
/**
 * Aggiunge i fogli di stile e gli script al back-end.
 *
 * @since 1.0.0
 */
function mst_admin_enqueue_scripts( $hook ) {
	/* Styles */
	/*wp_enqueue_style( 
		'unique_stylesheet_name', 
		MST_THEME_URL . 'assets/css/stylesheet.css', 
		false(for no dependencies)|array(), 
		filemtime( MST_THEME_DIR . 'assets/css/stylesheet.css' ), 
		'media|all' 
	);*/
	
	wp_enqueue_style( 
		'acf_admin_style', 
		MST_THEME_URL . 'assets/css/acf-admin.css', 
		false, 
		false, 
		'all' 
	);
	
	/*wp_enqueue_style( 
		'bootstrap', 
		'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"',
		false, 
		false, 
		'all' 
	);*/
	
	
	
	/* Scripts */
	/*wp_enqueue_script( 
		'unique_script_name', 
		MST_THEME_URL . 'assets/js/script.js', 
		false(for no dependencies)|array(), 
		filemtime( MST_THEME_DIR . 'assets/js/script.js' ),
		true(for script before </body>)|false 
	);*/
	
	/*wp_enqueue_script( 
		'acf_admin_script', 
		MST_THEME_URL . 'assets/js/acf-admin.js', 
		false, 
		false,
		true 
	);*/
	
}

add_action( 'admin_enqueue_scripts', 'mst_admin_enqueue_scripts', 10, 1 );