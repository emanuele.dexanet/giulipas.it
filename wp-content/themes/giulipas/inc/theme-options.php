<?php
/**
 * Aggiunge nuovi campi alle pagine di opzioni di WordPress e del tema padre.
 *
 * @since 1.0.0
 */
function mst_display_options() {
	/* Pagine di opzioni del tema padre */
	register_setting( 'footer_section', 'page_for_credits' );
	
	add_settings_field(
		'page_for_credits',
		__( 'Credits page', MST_THEME_DOMAIN ),
		'page_for_credits_callback_function',
		'theme-options',
		'footer_section'
	);
	
	
	
	register_setting( 'social_section', 'youtube_url' );
	
	add_settings_field(
		'youtube_url',
		'YouTube Profile Url',
		'youtube_url_callback_function',
		'theme-options',
		'social_section'
	);
	
	
	
	/* Pagine di opzioni di WordPress */
	register_setting( 
		'reading',
		'case_history_archive_page',
		array(
			'type'              => 'string', 
			'sanitize_callback' => 'sanitize_text_field',
			'default'           => NULL,
		)
	);

	add_settings_field(
		'case_history_archive_page',
		__( 'Case History Archive Page', MST_THEME_DOMAIN ),
		'case_history_archive_page_callback_function',
		'reading',
		'default',
		array( 
			'label_for' => 'case_history_archive_page' 
		)
	);
	
	
	
	register_setting( 
		'reading',
		'page_404',
		array(
			'type'              => 'string', 
			'sanitize_callback' => 'sanitize_text_field',
			'default'           => NULL,
		)
	);

	add_settings_field(
		'page_404',
		__( '404 Page', MST_THEME_DOMAIN ),
		'page_404_callback_function',
		'reading',
		'default',
		array( 
			'label_for' => 'page_404' 
		)
	);
}

add_action( 'admin_init', 'mst_display_options' );



/**
 * Render callback dell'opzione "page_for_credits".
 *
 * @since 1.0.0
 */
function page_for_credits_callback_function() {
	wp_dropdown_pages( array(
		'echo'              => 1,
		'name'              => 'page_for_credits',
		'id'                => 'page_for_credits',
		'selected'          => get_option( 'page_for_credits' ),
		'show_option_none' 	=> '— Seleziona —', 
		'option_none_value' => '0',
		'value_field'       => 'ID'
	) );
}



/**
 * Render callback dell'opzione "youtube_url".
 *
 * @since 1.0.0
 */
function youtube_url_callback_function() {
	?>

	<input placeholder="https://youtube.com/" type="text" name="youtube_url" id="youtube_url" value="<?php echo get_option('youtube_url'); ?>" />
	
	<?php
}



/**
 * Render callback dell'opzione "case_history_archive_page".
 *
 * @since 1.0.0
 */
function case_history_archive_page_callback_function() {
	wp_dropdown_pages( array(
		'echo'              => 1,
		'name'              => 'case_history_archive_page',
		'id'                => 'case_history_archive_page',
		'selected'          => get_option( 'case_history_archive_page' ),
		'show_option_none' 	=> '— Seleziona —', 
		'option_none_value' => '0',
		'value_field'       => 'ID'
	) );
}



/**
 * Render callback dell'opzione "page_404".
 *
 * @since 1.0.0
 */
function page_404_callback_function() {
	wp_dropdown_pages( array(
		'echo'              => 1,
		'name'              => 'page_404',
		'id'                => 'page_404',
		'selected'          => get_option( 'page_404' ),
		'show_option_none' 	=> '— Seleziona —', 
		'option_none_value' => '0',
		'value_field'       => 'ID'
	) );
}