<?php
if ( !function_exists( 'acf_register_block' ) ) {
	return;
}



/**
 * Aggiunge nuove categorie per raggruppare i blocchi.
 *
 * @since 1.0.0
 */
add_filter( 'block_categories', 'mst_block_categories', 10, 2 );

function mst_block_categories( $categories, $post ) {
	/*
	if ( $post->post_type !== 'post' ) {
		return $categories;
	}
	*/

	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'giulipas',
				'title' => __( 'Giulipas', MST_THEME_DOMAIN ),
			),
		)
	);
}



require_once( MST_THEME_DIR . 'inc/blocks/accordion.php' );
require_once( MST_THEME_DIR . 'inc/blocks/blog.php' );
require_once( MST_THEME_DIR . 'inc/blocks/button.php' );
require_once( MST_THEME_DIR . 'inc/blocks/case-history.php' );
require_once( MST_THEME_DIR . 'inc/blocks/column.php' );
require_once( MST_THEME_DIR . 'inc/blocks/contact-us.php' );
require_once( MST_THEME_DIR . 'inc/blocks/contacts-with-map.php' );
require_once( MST_THEME_DIR . 'inc/blocks/counters.php' );
require_once( MST_THEME_DIR . 'inc/blocks/cta.php' );
require_once( MST_THEME_DIR . 'inc/blocks/cut-line.php' );
require_once( MST_THEME_DIR . 'inc/blocks/downloads.php' );
require_once( MST_THEME_DIR . 'inc/blocks/full-width-image.php' );
require_once( MST_THEME_DIR . 'inc/blocks/hero.php' );
require_once( MST_THEME_DIR . 'inc/blocks/highlights.php' );
require_once( MST_THEME_DIR . 'inc/blocks/history.php' );
require_once( MST_THEME_DIR . 'inc/blocks/intro.php' );
require_once( MST_THEME_DIR . 'inc/blocks/list-group.php' );
require_once( MST_THEME_DIR . 'inc/blocks/picture-text-columns.php' );
require_once( MST_THEME_DIR . 'inc/blocks/pictures-in-a-row.php' );
require_once( MST_THEME_DIR . 'inc/blocks/row.php' );
require_once( MST_THEME_DIR . 'inc/blocks/section.php' );
require_once( MST_THEME_DIR . 'inc/blocks/separator.php' );
require_once( MST_THEME_DIR . 'inc/blocks/slideshow.php' );
require_once( MST_THEME_DIR . 'inc/blocks/three-columns.php' );
require_once( MST_THEME_DIR . 'inc/blocks/two-picture-columns.php' );
require_once( MST_THEME_DIR . 'inc/blocks/video.php' );
