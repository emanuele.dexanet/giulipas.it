<?php
/**
 * Genera l'oggetto relativo agli slider.
 * Viene utilizzato nel file "acf-block-slideshow.js.
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_acf_sliders_data_object() {
	$taxonomy = 'slider';
	$args     = array(
		'order'        => 'ASC',
		'orderby'      => 'name',
		'hide_empty'   => false,
		'fields'       => 'ids',
		'hierarchical' => true,
		'childless'    => false,
	);
	$terms    = get_terms( $taxonomy, $args );
	$object   = array();
	
	if ( $terms ) {
		foreach ( $terms as $term_id ) {
			$args   = array(
				'posts_per_page'   => -1,
				'offset'           => 0,
				'orderby'          => 'title',
				'order'            => 'ASC',
				'post_type'        => 'slide',
				'post_status'      => 'publish',
				'tax_query'        => array(
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'term_id',
						'terms'    => $term_id
					)
				)
			);
			$slides = get_posts( $args );
			
			if ( $slides ) {
				$object[ $term_id ] = array();
				
				foreach ( $slides as $slide ) {
					$object[ $term_id ][] = array(
						'value' => $slide->ID,
						'label' => $slide->post_title
					);
				}				
			}
		}
	}
	
	return $object;
}