<?php
/**
 * Registra il blocco "Contacts with Map".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-contacts-with-map',
	'title'	          => _x( 'Contacts with Map', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'contacts', 'map', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'auto',
	'render_callback' => 'mst_acf_block_contacts_with_map_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'enqueue_assets'  => 'mst_acf_block_contacts_with_map_enqueue_assets'
) );



/**
 * Render callback del blocco "Contacts with Map".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_contacts_with_map_render_callback( $block, $content = '', $is_preview = false ) {
	$context               = Timber::context();
	$context['fields']     = get_fields();
	$context['is_preview'] = $is_preview;
	
	if ( !array_key_exists( 'className', $block) ) {
		$block['className'] = '';
	}
	
	$context['block'] = $block;
	
	Timber::render( 'blocks/contacts-with-map.twig', $context );
}



/**
 * Enqueue scripts & styles per il blocco "Contacts with Map".
 *
 * @since 1.0.0
 */
function mst_acf_block_contacts_with_map_enqueue_assets() {
	if ( !is_admin() ) {
		wp_enqueue_script( 
			'googlemaps_api', 
			'https://maps.googleapis.com/maps/api/js?key=' . MAP_API_KEY . '&v=beta&callback=initMap', 
			array( 'jquery' ), 
			false, 
			true 
		);
		
		wp_enqueue_script( 
			'map_script', 
			MST_THEME_URL . 'assets/blocks/contacts-with-map/contacts-with-map.js', 
			array( 'jquery', 'googlemaps_api' ), 
			filemtime( MST_THEME_DIR . 'assets/blocks/contacts-with-map/contacts-with-map.js' ), 
			true 
		);
	}
}