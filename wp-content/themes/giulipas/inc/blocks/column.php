<?php
/**
 * Registra il blocco "Column".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-column',
	'title'	          => _x( 'Column', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'column', 'layout', 'giulipas' ),
	'post_types'      => array( 'page', 'post', 'case-history' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_column_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array('acf/mst-row'),
	//'enqueue_script'  => MST_THEME_URL . 'assets/blocks/column/column.js',
) );



/**
 * Render callback del blocco "Column".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_column_render_callback( $block, $content = '', $is_preview = false ) {
	$context               = Timber::context();
	$context['fields']     = get_fields();
	$context['is_preview'] = $is_preview;
	// $columns_classes è un array del tipo:
	//    Array
	//    (
	//       'wpb_grid_small'       => 
	//       'wpb_grid_medium'      => 
	//       'wpb_grid_large'       => 
	//       'wpb_grid_extra_large' => 
	//    )
	$column_classes        = get_field( 'wpb_grid_breakpoints' );
	$background_color      = get_field( 'wpb_background_color' );
	$text_color            = get_field( 'wpb_text_color' );
	$background_image      = get_field( 'wpb_background_image' );
	$parallax              = get_field( 'wpb_parallax' );
	$styles                = array();
	
	if ( !array_key_exists( 'className', $block) ) {
		$block['className'] = '';
	}
	
	if ( !empty( $column_classes ) ) {
		foreach ( $column_classes as $key => $value ) {
			if ( '' != $value ) {
				$block['className'] .= ' ' . $value; 
			}
		}
	} elseif ( !empty( $block['data'] ) ) {
		$grid_layout = array(
			'wpb_grid_breakpoints_wpb_grid_small', 
			'wpb_grid_breakpoints_wpb_grid_medium', 
			'wpb_grid_breakpoints_wpb_grid_large', 
			'wpb_grid_breakpoints_wpb_grid_extra_large'
		);
		
		foreach ( $block['data'] as $key => $value ) {
			if ( in_array( $key, $grid_layout ) && '' != $value ) {
				$block['className'] .= ' ' . $value; 
			}
		}
	}
	
	if ( $background_color ) {
		$block['className'] .= ' ' . sprintf( "bkg-%s", $background_color );
	}
	
	if ( $text_color ) {
		$styles[] = sprintf( "color: %s;", $text_color );
	}
	
	if ( $background_image ) {
		$block['className'] .= ' ' . 'background-image';
		$styles[]            = sprintf( "background-image: url('%s');", $background_image );
		
		if ( $parallax ) {
			$block['className'] .= ' ' . 'background-parallax';
		}
	}
	
	if ( !empty( $block['align'] ) ) {
		$block['className'] .= ' align-' . $block['align'];
	}
	
	if ( !empty( $block['align_text'] ) ) {
		$block['className'] .= ' align-text-' . $block['align_text'];
	}
	
	if ( !empty( $block['align_content'] ) ) {
		$block['className'] .= ' align-content-' . $block['align_content'];
	}
	
	$context['styles'] = implode( ' ', $styles );
	$context['block']  = $block;
	
	Timber::render( 'blocks/column.twig', $context );
}