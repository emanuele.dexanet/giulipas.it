<?php
/**
 * Registra il blocco "Pictures in a row".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-pictures-in-a-row',
	'title'	          => _x( 'Pictures in a row', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'pictures', 'images', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_pictures_in_a_row_render_callback',
	'supports'        => array(
		'align'         => false,
		'align_text'    => false,
		'align_content' => false,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array( 'acf/mst-row' )
) );



/**
 * Render callback del blocco "Pictures in a row".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_pictures_in_a_row_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array( 
			'acf/mst-column', 
			array(
				'className' => 'd-flex flex-row justify-content-around flex-wrap',
				'data'      => array(
					'wpb_grid_breakpoints_wpb_grid_small' => 'col-12'
				)
			), 
			array(
				array(
					'acf/mst-pictures-in-a-row-child',
					array()
				)
			)
		)
	);
	$allowed_blocks            = array( 'acf/mst-column', 'acf/mst-pictures-in-a-row-child' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );
	
	Timber::render( 'blocks/pictures-in-a-row.twig', $context );
}



/**
 * Registra il blocco "Pictures in a row child".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-pictures-in-a-row-child',
	'title'	          => _x( 'Pictures in a row child', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'pictures', 'images', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_pictures_in_a_row_child_render_callback',
	'supports'        => array(
		'align'         => false,
		'align_text'    => false,
		'align_content' => false,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array( 'acf/mst-pictures-in-a-row' )
) );



/**
 * Render callback del blocco "Pictures in a row".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_pictures_in_a_row_child_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array(
			'core/image',
			array()
		)
	);
	$allowed_blocks            = array( 'core/image' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );
	
	Timber::render( 'blocks/pictures-in-a-row-child.twig', $context );
}