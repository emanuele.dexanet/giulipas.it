<?php
/**
 * Registra il blocco "Button".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-button',
	'title'	          => _x( 'Button', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'button', 'link', 'icon link' ),
	'post_types'      => array( 'page', 'post', 'case-history' ),
	'mode'            => 'auto',
	'render_callback' => 'mst_acf_block_button_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	)
) );



/**
 * Render callback del blocco "Button".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_button_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$button_style              = get_field( 'wpb_button_style' );
	$button_type               = 'button';
	
	switch ( $button_style ) {
		case 'ghost':
			$wrapper_class = 'is-style-ghost-default';
			$class         = '';
			break;
		case 'ghost-primary':
			$wrapper_class = 'is-style-ghost-primary';
			$class         = '';
			break;
		case 'default':
			$wrapper_class = 'is-style-filled-default';
			$class         = '';
			break;
		case 'primary':
			$wrapper_class = 'is-style-filled-primary';
			$class         = '';
			break;
		case 'arrow-right':
			$wrapper_class = '';
			$class         = '';
			$button_type   = 'icon';
			break;
		default:
			$wrapper_class = 'is-style-ghost-default';
			$class         = '';
	}
	
	$context['wrapper_class'] = $wrapper_class;
	$context['class']         = $class;
	$context['button_type']   = $button_type;
	
	Timber::render( 'blocks/button.twig', $context );
}