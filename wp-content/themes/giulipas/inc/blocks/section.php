<?php
/**
 * Registra il blocco "Section".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-section',
	'title'	          => _x( 'Section', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'section', 'layout', 'giulipas' ),
	'post_types'      => array( 'page', 'post', 'case-history' ),
	'mode'            => 'preview', // 'preview|auto|edit'
//	'render_template' => MST_THEME_URL . 'relative_path',
	'render_callback' => 'mst_acf_block_section_render_callback',
//	'enqueue_style'   => MST_THEME_URL . 'relative_path',
//	'enqueue_script'  => MST_THEME_URL . 'relative_path',
//	'enqueue_assets'  => 'mst_acf_block_enqueue_assets'
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	/*'enqueue_script'  => MST_THEME_URL . 'assets/blocks/section/section.js',*/
) );



/**
 * Render callback del blocco "Section".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_section_render_callback( $block, $content = '', $is_preview = false ) {
	$context               = Timber::context();
	$context['fields']     = get_fields();
	$context['is_preview'] = $is_preview;
	$content_width         = get_field( 'wpb_content_width' );
	$container_class       = get_field( 'wpb_content_container_class' );
	$background_color      = get_field( 'wpb_background_color' );
	$text_color            = get_field( 'wpb_text_color' );
	$background_image      = get_field( 'wpb_background_image' );
	$parallax              = get_field( 'wpb_parallax' );
	$styles                = array();
	
	if ( !array_key_exists( 'className', $block) ) {
		$block['className'] = '';
	}
	
	if ( empty( $content_width ) && !empty( $block['data'] ) && array_key_exists( 'wpb_content_width', $block['data'] ) ) {
		$context['fields']['wpb_content_width'] = $block['data']['wpb_content_width'];
	}
	
	if ( empty( $container_class ) && !empty( $block['data'] ) && array_key_exists( 'wpb_content_container_class', $block['data'] ) ) {
		$context['fields']['wpb_content_container_class'] = $block['data']['wpb_content_container_class'];
	}
	
	if ( !empty( $background_color ) ) {
		$block['className'] .= ' ' . sprintf( "bkg-%s", $background_color );
	} elseif ( !empty( $block['data'] ) && array_key_exists( 'wpb_background_color', $block['data'] ) ) {
		$block['className'] .= ' ' . sprintf( "bkg-%s", $block['data']['wpb_background_color'] );
	}
	
	if ( !empty( $text_color ) ) {
		$styles[] = sprintf( "color: %s;", $text_color );
	}
	
	if ( $background_image ) {
		$block['className'] .= ' ' . 'background-image';
		$styles[]            = sprintf( "background-image: url('%s');", $background_image );
		
		if ( $parallax ) {
			$block['className'] .= ' ' . 'background-parallax';
		}
	}
	
	$context['styles'] = implode( ' ', $styles );
	$context['block']  = $block;
	
	Timber::render( 'blocks/section.twig', $context );
}