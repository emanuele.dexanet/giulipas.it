
<?php
/**
 * Registra il blocco "Intro post".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-intro-post',
	'title'	          => _x( 'Intro post', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'intro', 'giulipas' ),
	'post_types'      => array( 'post' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_intro_post_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	)
) );



/**
 * Render callback del blocco "Intro post".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_intro_post_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
    $timber_post               = Timber::get_post( get_the_ID() );
    $template                  = array(
		array(
			'core/heading',
			array(
				'level'       => 1,
				'className'   => '',
				'content'     => $timber_post->post_title,
			)
		)
	);
	$allowed_blocks            = array( 'core/heading' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );
    $context['post']           = $timber_post;

	Timber::render( 'blocks/intro-post.twig', $context );
}
