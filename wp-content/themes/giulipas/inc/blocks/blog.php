<?php
/**
 * Registra il blocco "Blog preview".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-blog-preview',
	'title'	          => _x( 'Blog preview', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'blog', 'post', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_blog_preview_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'example'         => array(
		'attributes' => array(
			'mode' => 'preview',
			'data' => array(
				'heading'            => 'News bottom of the hierarchy, the size of the volume is just.',
				'button_placeholder' => 'Archivio news',
				'button_url'         => get_post_type_archive_link( 'post' )
			)
		)
	)
) );



/**
 * Render callback del blocco "Blog preview".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_blog_preview_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array(
			'acf/mst-section',
			array(
				'className' => 'blog-preview',
			),
			array(
				array(
					'acf/mst-row',
					array(
						'className' => 'mb-3',
					),
					array(
						array(
							'acf/mst-column',
							array(
								'className' => '',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small'       => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_large'       => 'col-lg-7',
									'wpb_grid_breakpoints_wpb_grid_extra_large' => 'col-xl-6'
								)
							),
							array(
								array(
									'core/heading',
									array(
										'level'     => 2,
										'classname' => '',
										'content'   => $block['example']['attributes']['data']['heading']
									)
								)
							)
						),
						array(
							'acf/mst-column',
							array(
								'className' => 'd-flex align-items-end justify-content-center justify-content-md-end',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small'       => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_large'       => 'col-lg-5',
									'wpb_grid_breakpoints_wpb_grid_extra_large' => 'col-xl-6'
								)
							),
							array(
								array(
									'core/button',
									array(
										'placeholder' => $block['example']['attributes']['data']['button_placeholder'],
										'url'         => $block['example']['attributes']['data']['button_url']
									)
								)
							)
						)
					)
				),
				array(
					'acf/mst-blog-preview-child',
					array()
				)
			)
		)
	);
	$allowed_blocks            = array( 'acf/mst-section', 'acf/mst-row', 'acf/mst-column', 'core/heading', 'core/button', 'acf/mst-blog-preview-child' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );

	Timber::render( 'blocks/blog-preview.twig', $context );
}



/**
 * Registra il blocco "Blog preview child".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-blog-preview-child',
	'title'	          => _x( 'Blog preview child', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'blog', 'post', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_blog_preview_child_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array( 'acf/mst-section' )
) );



/**
 * Render callback del blocco "Blog preview child".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_blog_preview_child_render_callback( $block, $content = '', $is_preview = false ) {
	$context               = Timber::context();
	$context['block']      = $block;
	$context['fields']     = get_fields();
	$context['is_preview'] = $is_preview;
	$args                  = array(
		'posts_per_page'      => get_field( 'wpb_blog_preview_numberposts' ),
		'post_type'           => 'post',
		'order'               => 'DESC',
		'ordrby'              => 'date',
		'ignore_sticky_posts' => 1
	);
	$context['posts']      = Timber::get_posts( $args );

	Timber::render( 'blocks/blog-preview-child.twig', $context );
}



/**
 * Registra il blocco "Blog archive".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-blog-archive',
	'title'	          => _x( 'Blog archive', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-post',
	'keywords'        => array( 'blog', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_blog_archive_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	)
) );



/**
 * Render callback del blocco "Blog archive".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_blog_archive_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array(
			'acf/mst-section',
			array(
				'className' => 'blog pt-0 pb-2',
				'data'      => array(
					'wpb_content_width' => 'container-fluid'
				)
			),
			array(
				array(
					'acf/mst-blog-archive-child',
					array()
				)
			)
		)
	);
	$allowed_blocks            = array( 'acf/mst-section', 'acf/mst-blog-archive-child' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );

	Timber::render( 'blocks/blog-archive.twig', $context );
}



/**
 * Registra il blocco "Blog archive child".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-blog-archive-child',
	'title'	          => _x( 'Blog archive child', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-post',
	'keywords'        => array( 'blog', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'auto',
	'render_callback' => 'mst_acf_block_blog_archive_child_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array( 'acf/mst-blog-archive' ),
	'enqueue_assets'  => 'mst_acf_block_blog_archive_child_enqueue_assets'
) );



/**
 * Render callback del blocco "Blog archive child".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_blog_archive_child_render_callback( $block, $content = '', $is_preview = false ) {
	$context                  = Timber::context();
	$context['block']         = $block;
	$context['fields']        = get_fields();
	$context['is_preview']    = $is_preview;
	$loading_posts_rule       = get_field( 'wpb_loading_posts_rule' );
	$per_page                 = ( 'numberposts' == $loading_posts_rule ) ? get_field( 'wpb_blog_numberposts' ) : get_field( 'wpb_blog_monthsnum' );
	$blog_posts               = mst_get_blog_posts( $loading_posts_rule, $per_page );
	$posts                    = $blog_posts['posts'];
	$show_loadmore            = $blog_posts['show_loadmore'];
	$groups                   = mst_group_posts( $posts );
	$context['rule']          = $loading_posts_rule;
	$context['per_page']      = $per_page;
	$context['groups']        = $groups;
	$context['show_loadmore'] = $show_loadmore;
	$context['nonce']         = wp_create_nonce( 'loadmore-' . $block['id'] );

	Timber::render( 'blocks/blog-archive-child.twig', $context );
}



/**
 * Enqueue scripts & styles per il blocco "Blog archive child".
 *
 * @since 1.0.0
 */
function mst_acf_block_blog_archive_child_enqueue_assets() {
	if ( !is_admin() ) {
		wp_enqueue_script(
			'mst_load_more_posts',
			MST_THEME_URL . 'assets/blocks/blog-archive/blog-archive.js',
			array( 'jquery' ),
			filemtime( MST_THEME_DIR . 'assets/blocks/blog-archive/blog-archive.js' ),
			true
		);

		wp_localize_script(
			'mst_load_more_posts',
			'mst_blog_vars',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
			)
		);
	}
}
