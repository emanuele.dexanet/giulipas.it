<?php
/**
 * Registra il blocco "History".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-history',
	'title'	          => _x( 'History', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'history', 'timeline', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_history_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'example'         => array(
		'attributes' => array(
			'mode' => 'preview',
			'data' => array(
				'heading'   => 'Storia.',
				'paragraph' => 'Vestibulum mi Leo, elementum eu dictum sed, lobortis sed massa. Aliquam sit amet.'
			)
		)
	)
) );



/**
 * Render callback del blocco "History".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_history_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array( 
			'acf/mst-section', 
			array(
				'className' => 'history',
			), 
			array(
				array( 
					'acf/mst-row', 
					array(
						'className' => 'mb-2',
					), 
					array(
						array( 
							'acf/mst-column', 
							array(
								'className' => '',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small' => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_large' => 'col-lg-5'
								)
							), 
							array(
								array( 
									'core/heading', 
									array(
										'level'     => 2,
										'classname' => 'mb-1',
										'content'   => $block['example']['attributes']['data']['heading']
									) 
								),
								array(
									'core/paragraph',
									array(
										'content' => $block['example']['attributes']['data']['paragraph']
									)
								)
							)
						),
						array( 
							'acf/mst-column', 
							array(
								'className' => 'offset-lg-1',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small' => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_large' => 'col-lg-6'
								)
							), 
							array(
								array( 
									'acf/mst-history-child', 
									array()
								)
							)
						)
					)
				)
			)
		)
	);
	$allowed_blocks            = array( 'acf/mst-section', 'acf/mst-row', 'acf/mst-column', 'core/heading', 'core/paragraph', 'acf/mst-history-child' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );
	
	Timber::render( 'blocks/history.twig', $context );
}



/**
 * Registra il blocco "History child".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-history-child',
	'title'	          => _x( 'History child', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'history', 'timeline', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'auto',
	'render_callback' => 'mst_acf_block_history_child_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array( 'acf/mst-column', 'acf/mst-history'),
) );



/**
 * Render callback del blocco "History child".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_history_child_render_callback( $block, $content = '', $is_preview = false ) {
	$context               = Timber::context();
	$context['block']      = $block;
	$context['fields']     = get_fields();
	$context['is_preview'] = $is_preview;
	
	Timber::render( 'blocks/history-child.twig', $context );
}