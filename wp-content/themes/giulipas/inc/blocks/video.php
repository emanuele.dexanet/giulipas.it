<?php
/**
 * Registra il blocco "Video".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-video',
	'title'	          => _x( 'Video', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'video', 'giulipas' ),
	'post_types'      => array( 'page', 'post', 'case-history' ),
	'mode'            => 'auto',
	'render_callback' => 'mst_acf_block_video_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	)
) );



/**
 * Render callback del blocco "Video".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_video_render_callback( $block, $content = '', $is_preview = false ) {
	$context               = Timber::context();
	$context['fields']     = get_fields();
	$context['is_preview'] = $is_preview;
	$background_image      = get_field( 'wpb_background_image' );
	$parallax              = get_field( 'wpb_parallax' );
	$hide_related          = get_field( 'wpb_video_rel' );
	$hide_info             = get_field( 'wpb_video_showinfo' );
	$video_url             = get_field( 'wpb_video_url' );
	$styles                = array();
	
	if ( !array_key_exists( 'className', $block) ) {
		$block['className'] = '';
	}
	
	if ( $background_image ) {
		$block['className'] .= ' ' . 'background-image';
		$styles[]            = sprintf( "background-image: url('%s');", $background_image );
		
		if ( $parallax ) {
			$block['className'] .= ' ' . 'background-parallax';
		}
	} else {
		$block['className'] .= ' ' . 'bkg-medium-grey';
	}
	
	if ( $video_url ) {
		$query = parse_url( $video_url , PHP_URL_QUERY );
		
		parse_str( $query, $params );
		
		$params['rel']        = $hide_related ? 0 : 1;
		$params['showinfo']   = $hide_info ? 0 : 1;
		$context['video_src'] = sprintf( '%1$s?%2$s', strtok( $video_url, '?' ), http_build_query( $params ) );
	} else {
		$context['video_src'] = '';
	}
	
	$context['styles'] = implode( ' ', $styles );
	$context['block']  = $block;
	
	Timber::render( 'blocks/video.twig', $context );
}