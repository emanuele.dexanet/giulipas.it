<?php
/**
 * Registra il blocco "Hero".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-hero',
	'title'	          => _x( 'Hero', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'hero', 'section' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'auto',
	'render_callback' => 'mst_acf_block_hero_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
) );



/**
 * Render callback del blocco "Hero".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_hero_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	/*$template                  = array(
		array(
			'acf/mst-section',
			array(
				'className' => 'hero-section full-width pt-0 pb-0 d-flex',
				'data'      => array(
					'wpb_content_width' => 'container-fluid'
				)
			),
			array(
				array(
					'acf/mst-row',
					array(
						'className' => 'd-flex h-100'
					),
					array(
						array(
							'acf/mst-column',
							array(
								'className' => 'hero-content d-flex flex-column justify-content-center',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small' => 'col-12',
								)
							),
							array(
								array(
									'acf/mst-row',
								),
								array(),
								array(
									array(
										'acf/mst-column',
										array(
											'className' => 'offset-md-3',
											'data'      => array(
												'wpb_grid_breakpoints_wpb_grid_small'  => 'col-12',
												'wpb_grid_breakpoints_wpb_grid_medium' => 'col-md-6',
											)
										),
										array(
											array(
												'acf/mst-page-title',
												array()
											)
										)
									)
								)
							)
						)
					)
				),
				array(
					'acf/mst-scroll-down',
					array()
				)
			)
		)
	);

	$allowed_blocks            = array( 'acf/mst-section', 'acf/mst-row', 'acf/mst-column', 'acf/mst-page-title', 'acf/mst-scroll-down' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );*/

	$background_color      = get_field( 'wpb_background_color' );
	$background_type       = get_field( 'wpb_background_type' );
	$classes               = array();
	$styles                = array();

	switch ( $background_type ) {
		case 'image':
			$background_image = get_field( 'wpb_background_image' );
			$parallax         = get_field( 'wpb_parallax' );

			if ( $background_image ) {
				$classes[] = 'background-image';
				$styles[]  = sprintf( "background-image: url('%s');", $background_image );

				if ( $parallax ) {
					$classes[] = 'background-parallax';
				}
			}
			break;
		case 'video':
			$background_video = get_field( 'wpb_video' );
			$poster           = get_field( 'wpb_poster' );
			$classes[]        = 'background-video';

			if ( $background_video ) {
				$context['video'] = $background_video;
			}

			if ( $poster ) {
				$context['poster'] = $poster;
			}
			break;
	}

	if ( $background_color ) {
		$classes[] = sprintf( "bkg-%s", $background_color );
	}

	$context['classes'] = implode( ' ', $classes );
	$context['styles']  = implode( ' ', $styles );
	$context['block']   = $block;

	Timber::render( 'blocks/hero.twig', $context );
}
