<?php
/**
 * Registra il blocco "Case History filtered".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-ch-filtered',
	'title'	          => _x( 'Case History filtered', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'open-folder',
	'keywords'        => array( 'case history', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_ch_filtered_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'example'         => array(
		'attributes' => array(
			'mode' => 'preview',
			'data' => array(
				'heading'            => 'Case History',
				'button_placeholder' => 'Archivio Case History',
				'button_url'         => get_post_type_archive_link( 'case-history' )
			)
		)
	)
) );



/**
 * Render callback del blocco "Case History filtered".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_ch_filtered_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array(
			'acf/mst-section',
			array(
				'className' => 'case-history-preview',
				'data'      => array(
					'wpb_content_width' => 'container-fluid',
				)
			),
			array(
				array(
					'acf/mst-row',
					array(
						'className' => 'mb-3'
					),
					array(
						array(
							'acf/mst-column',
							array(
								'className' => '',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small'  => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_medium' => 'col-md-7',
									'wpb_grid_breakpoints_wpb_grid_large'  => 'col-lg-8'
								)
							),
							array(
								array(
									'core/heading',
									array(
										'level'     => 2,
										'className' => '',
										'content'   => $block['example']['attributes']['data']['heading']
									)
								)
							)
						),
						array(
							'acf/mst-column',
							array(
								'className' => 'text-right',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small'  => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_medium' => 'col-md-5',
									'wpb_grid_breakpoints_wpb_grid_large'  => 'col-lg-4'
								)
							),
							array(
								array(
									'core/button',
									array(
										'className'   => '',
										'placeholder' => $block['example']['attributes']['data']['button_placeholder'],
										'url'         => $block['example']['attributes']['data']['button_url']
									)
								)
							)
						)
					)
				),
				array(
					'acf/mst-ch-filtered-child',
					array(),
				)
			)
		)
	);
	$allowed_blocks            = array( 'acf/mst-section', 'acf/mst-row', 'acf/mst-column', 'core/heading', 'acf/mst-ch-filtered-child' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );

	Timber::render( 'blocks/case-history-filtered.twig', $context );
}



/**
 * Registra il blocco "Case History filtered child".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-ch-filtered-child',
	'title'	          => _x( 'Case History filtered child', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'open-folder',
	'keywords'        => array( 'case history', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_ch_filtered_child_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array( 'mst-ch-filtered' )
) );



/**
 * Render callback del blocco "Case History filtered child".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_ch_filtered_child_render_callback( $block, $content = '', $is_preview = false ) {
	$context               = Timber::context();
	$context['block']      = $block;
	$context['fields']     = get_fields();
	$context['is_preview'] = $is_preview;
	$ch_posts              = mst_get_case_history_posts( -1, get_field( 'wpb_case_history_category' ), 1 );
	$posts                 = $ch_posts['posts'];
	$show_loadmore         = $ch_posts['show_loadmore'];
	$context['posts']      = $posts;

	Timber::render( 'blocks/case-history-filtered-child.twig', $context );
}



/**
 * Registra il blocco "Case History archive".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-case-history-archive',
	'title'	          => _x( 'Case History archive', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-post',
	'keywords'        => array( 'case history', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_case_history_archive_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	)
) );



/**
 * Render callback del blocco "Case History archive".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_case_history_archive_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array(
			'acf/mst-section',
			array(
				'className' => 'case-history',
				'data'      => array(
					'wpb_content_width' => 'container-fluid'
				)
			),
			array(
				array(
					'acf/mst-case-history-archive-child',
					array()
				)
			)
		)
	);
	$allowed_blocks            = array( 'acf/mst-section', 'acf/mst-case-history-archive-child' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );

	Timber::render( 'blocks/case-history-archive.twig', $context );
}



/**
 * Registra il blocco "Case History archive child".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-case-history-archive-child',
	'title'	          => _x( 'Case History archive child', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-post',
	'keywords'        => array( 'case-history', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'auto',
	'render_callback' => 'mst_acf_block_case_history_archive_child_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'parent'          => array( 'acf/mst-case-history-archive' ),
	'enqueue_assets'  => 'mst_acf_block_case_history_archive_child_enqueue_assets'
) );



/**
 * Render callback del blocco "Case History archive child".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_case_history_archive_child_render_callback( $block, $content = '', $is_preview = false ) {
	$context                  = Timber::context();
	$context['block']         = $block;
	$context['fields']        = get_fields();
	$context['is_preview']    = $is_preview;
	$ch_category_terms        = get_terms( array(
		'taxonomy'     => 'ch-category',
		'orderby'      => 'name',
		'order'        => 'ASC',
		'hide_empty'   => false,
		'hierarchical' => true,
	) );
	$first_term_id            = ( !empty( $ch_category_terms ) ) ? $ch_category_terms[0]->term_id : 0;
	$context['ch_category']   = $ch_category_terms;
	//$ch_posts                 = mst_get_case_history_posts( 3, $first_term_id, 1 );
	$ch_posts                 = mst_get_case_history_posts( 3, 0, 1 );
	$posts                    = $ch_posts['posts'];
	$show_loadmore            = $ch_posts['show_loadmore'];
	$context['posts']         = $posts;
	$context['show_loadmore'] = $show_loadmore;
	$context['nonce']         = wp_create_nonce( 'loadmore-' . $block['id'] );
	$detect                   = new Mobile_Detect;
	$context['is_mobile']     = $detect->isMobile();
	$context['is_tablet']     = $detect->isTablet();

	Timber::render( 'blocks/case-history-archive-child.twig', $context );
}



/**
 * Enqueue scripts & styles per il blocco "Case History archive child".
 *
 * @since 1.0.0
 */
function mst_acf_block_case_history_archive_child_enqueue_assets() {
	if ( !is_admin() ) {
		wp_enqueue_script(
			'mst_load_more_ch',
			MST_THEME_URL . 'assets/blocks/case-history-archive/case-history-archive.js',
			array( 'jquery' ),
			filemtime( MST_THEME_DIR . 'assets/blocks/case-history-archive/case-history-archive.js' ),
			true
		);

		wp_localize_script(
			'mst_load_more_ch',
			'mst_ch_vars',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
			)
		);
	}
}
