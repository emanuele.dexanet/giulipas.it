
<?php
/**
 * Registra il blocco "Intro".
 *
 * @since 1.0.0
 */
acf_register_block( array(
	'name'            => 'mst-intro',
	'title'	          => _x( 'Intro', 'custom block', MST_THEME_DOMAIN ),
	'description'     => '',
	'category'        => 'giulipas',
	'icon'            => '<svg enable-background="new 0 0 74.7 49.9" viewBox="0 0 74.7 49.9" xmlns="http://www.w3.org/2000/svg"><path d="m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"/></svg>',//'admin-site-alt',
	'keywords'        => array( 'intro', 'giulipas' ),
	'post_types'      => array( 'page' ),
	'mode'            => 'preview',
	'render_callback' => 'mst_acf_block_intro_render_callback',
	'supports'        => array(
		'align'         => true,
		'align_text'    => true,
		'align_content' => true,
		'mode'          => true,
		'multiple'      => true,
		'jsx'           => true,
	),
	'example'         => array(
		'attributes' => array(
			'mode' => 'preview',
			'data' => array(
				'heading'   => 'Titolo pagina',
				'paragraph' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec accumsan dolor. Nunc at purus eget ante malesuada facilisis. Proin tempus interdum enim. Quisque ornare libero urna, eu porttitor ex tincidunt non. Vestibulum a velit massa.',
			)
		)
	)
) );



/**
 * Render callback del blocco "Intro".
 *
 * @param {array}  $block      : the block settings and attributes
 * @param {string} $content    : the block content (emtpy string)
 * @param {bool}   $is_preview : true during AJAX preview
 *
 * @since 1.0.0
 */
function mst_acf_block_intro_render_callback( $block, $content = '', $is_preview = false ) {
	$context                   = Timber::context();
	$context['block']          = $block;
	$context['fields']         = get_fields();
	$context['is_preview']     = $is_preview;
	$template                  = array(
		array( 
			'acf/mst-section', 
			array(
				'className' => '',
				'data'      => array(
					'wpb_content_width' => 'container-fluid',
				)
			), 
			array(
				array( 
					'acf/mst-row', 
					array(), 
					array(
						array( 
							'acf/mst-column', 
							array(
								'className' => '',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small'  => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_medium' => 'col-lg-4'
								)
							), 
							array(
								array( 
									'core/heading', 
									array(
										'level'       => 1,
										'className'   => '',
										'placeholder' => $block['example']['attributes']['data']['heading']
									) 
								)
							)
						),
						array( 
							'acf/mst-column', 
							array(
								'className' => 'd-none d-lg-block',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small'  => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_medium' => 'col-lg-2'
								)
							), 
							array(
								array( 
									'acf/mst-cut-line', 
									array() 
								)
							)
						),
						array( 
							'acf/mst-column', 
							array(
								'className' => 'mt-lg-3',
								'data'      => array(
									'wpb_grid_breakpoints_wpb_grid_small'  => 'col-12',
									'wpb_grid_breakpoints_wpb_grid_medium' => 'col-lg-6'
								)
							), 
							array(
								array( 
									'core/paragraph', 
									array(
										'className'   => '',
										'placeholder' => $block['example']['attributes']['data']['paragraph']
									) 
								)
							)
						)
					)
				)
			)
		)
	);
	$allowed_blocks            = array( 'acf/mst-section', 'acf/mst-row', 'acf/mst-column', 'core/heading', 'core/paragraph', 'acf/mst-cut-line' );
	$context['template']       = esc_attr( wp_json_encode( $template ) );
	$context['allowed_blocks'] = esc_attr( wp_json_encode( $allowed_blocks ) );

	Timber::render( 'blocks/intro.twig', $context );
}