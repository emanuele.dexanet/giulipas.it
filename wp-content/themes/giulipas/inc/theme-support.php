<?php
// Add theme support for selective refresh for widgets.
add_theme_support( 'customize-selective-refresh-widgets' );


// Add support for Block Styles.
add_theme_support( 'wp-block-styles' );


// Add support for full and wide align images.
add_theme_support( 'align-wide' );


// Add support for editor styles.
add_theme_support( 'editor-styles' );
add_editor_style( array(
	'assets/css/editor-style.css'
) );


add_theme_support( 'disable-custom-font-sizes' );


// Editor color palette.
$white           = '#FFFFFF';
$very_light_grey = '#F7F7F7';
$light_grey      = '#D3D3D3';
$medium_grey     = '#707070';
$dark_grey       = '#464545';
$almost_black    = '#1C1C1C';
$black           = '#000000';
$light_blue      = '#0090DF';
$giulipas_blue   = '#1342F0';

add_theme_support( 'disable-custom-colors' );
add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'White', MST_THEME_DOMAIN ),
		'slug'  => 'white',
		'color'	=> $white,
	),
	array(
		'name'  => __( 'Very light grey', MST_THEME_DOMAIN ),
		'slug'  => 'very-light-grey',
		'color'	=> $very_light_grey,
	),
	array(
		'name'  => __( 'Light Grey', MST_THEME_DOMAIN ),
		'slug'  => 'light-grey',
		'color'	=> $light_grey,
	),
	array(
		'name'  => __( 'Medium Grey', MST_THEME_DOMAIN ),
		'slug'  => 'medium-grey',
		'color'	=> $medium_grey,
	),
	array(
		'name'  => __( 'Dark Grey', MST_THEME_DOMAIN ),
		'slug'  => 'dark-grey',
		'color'	=> $dark_grey,
	),
	array(
		'name'  => __( 'Almost Black', MST_THEME_DOMAIN ),
		'slug'  => 'almost-black',
		'color'	=> $almost_black,
	),
	array(
		'name'  => __( 'Black', MST_THEME_DOMAIN ),
		'slug'  => 'black',
		'color'	=> $black,
	),
	array(
		'name'  => __( 'Light Blue', MST_THEME_DOMAIN ),
		'slug'  => 'light-blue',
		'color'	=> $light_blue,
	),
	array(
		'name'  => __( 'Giulipas Blue', MST_THEME_DOMAIN ),
		'slug'  => 'giulipas-blue',
		'color'	=> $giulipas_blue,
	),
) );


// Add support for responsive embedded content.
add_theme_support( 'responsive-embeds' );


// Add support for custom line height controls.
add_theme_support( 'custom-line-height' );
