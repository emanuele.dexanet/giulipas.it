<?php
/**
 * Rimuove gli script del tema padre.
 *
 * @since 1.0.0
 */
function mst_wp_dequeue_script() {
	wp_dequeue_script( 'Popper' );
	wp_deregister_script( 'Popper' );
	wp_dequeue_script( 'Boostrap-min' );
	wp_deregister_script( 'Boostrap-min' );
}

//add_action( 'wp_print_scripts', 'mst_wp_dequeue_script', 100 );



/**
 * Aggiunge i fogli di stile e gli script al front-end.
 *
 * @since 1.0.0
 */
function mst_wp_enqueue_scripts() {
	/* Styles */
	/*wp_enqueue_style(
		'unique_stylesheet_name',
		MST_THEME_URL . 'assets/css/stylesheet.css',
		false(for no dependencies)|array(),
		filemtime( MST_THEME_DIR . 'assets/css/stylesheet.css' ),
		'media|all'
	);*/

	/* Slick */
	wp_enqueue_style(
		'slick_style',
		'//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css',
		false,
		'1.8.1',
		'all'
	);

	wp_enqueue_style(
		'slick_custom_style',
		MST_THEME_URL . 'assets/css/custom-slick.css',
		false,
		filemtime( MST_THEME_DIR . 'assets/css/custom-slick.css' ),
		'all'
	);


	/* Bootstrap Select */
	wp_enqueue_style(
		'bootstrap_select_style',
		'https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css',
		false,
		'1.13.14',
		'all'
	);


	/* Hover */
	wp_enqueue_style(
		'hover_style',
		MST_THEME_URL . 'assets/css/hover.css',
		false,
		filemtime( MST_THEME_DIR . 'assets/css/hover.css' ),
		'all'
	);


	/* Definisco la priorità di caricamento degli stili. */
	//_log($GLOBALS['CSS']);
	$parent_handles = array();

	foreach ($GLOBALS['CSS'] as $nome => $percorso) {
		$parent_handles[] = $nome;

		wp_enqueue_style( $nome, $percorso );
	}

	$parent_handles[] = 'default';

	wp_enqueue_style( 'default', get_template_directory_uri() . '/assets/custom.css' );


	wp_enqueue_style(
		'icons_style',
		MST_THEME_URL . 'assets/css/icons.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/icons.css' ),
		'all'
	);

	wp_enqueue_style(
		'fonts_style',
		MST_THEME_URL . 'assets/css/fonts.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/fonts.css' ),
		'all'
	);

	wp_enqueue_style(
		'custom_bootstrap_style',
		MST_THEME_URL . 'assets/css/custom-bootstrap.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/custom-bootstrap.css' ),
		'all'
	);

	wp_enqueue_style(
		'header_style',
		MST_THEME_URL . 'assets/css/header.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/header.css' ),
		'all'
	);

	wp_enqueue_style(
		'main_content_style',
		MST_THEME_URL . 'assets/css/main-content.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/main-content.css' ),
		'all'
	);

	wp_enqueue_style(
		'footer_style',
		MST_THEME_URL . 'assets/css/footer.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/footer.css' ),
		'all'
	);

	wp_enqueue_style(
		'forms_style',
		MST_THEME_URL . 'assets/css/forms.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/forms.css' ),
		'all'
	);

	wp_enqueue_style(
		'slideshow_style',
		MST_THEME_URL . 'assets/css/slideshow.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/slideshow.css' ),
		'all'
	);

	wp_enqueue_style(
		'giulipas_style',
		MST_THEME_URL . 'assets/css/giulipas.css',
		$parent_handles,
		filemtime( MST_THEME_DIR . 'assets/css/giulipas.css' ),
		'all'
	);



	/* Scripts */
	/*wp_enqueue_script(
		'unique_script_name',
		MST_THEME_URL . 'assets/js/script.js',
		false(for no dependencies)|array(),
		filemtime( MST_THEME_DIR . 'assets/js/script.js' ),
		true(for script before </body>)|false
	);*/


	/* Waypoint */
	wp_enqueue_script(
		'waypoint_script',
		MST_THEME_URL . 'assets/js/jquery.waypoints.min.js',
		array( 'jquery' ),
		false,
		true
	);


	/* Slick */
	wp_enqueue_script(
		'slick_script',
		'//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',
		array( 'jquery' ),
		'1.8.1',
		true
	);


	/* Bootstrap */
	/*wp_enqueue_script(
		'bootstrap',
		'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js',
		array( 'jquery' ),
		'4.6.0',
		true
	);*/


	/* Bootstrap Select */
	wp_enqueue_script(
		'bootstrap_select_script',
		'https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js',
		array( 'jquery', 'Popper', 'Boostrap-min' ),
		/*array( 'jquery', 'boostrap' ), */
		'1.13.14',
		true
	);


	/* Bootstrap File Input */
	wp_enqueue_script(
		'bootstrap_file_input_script',
		MST_THEME_URL . 'assets/js/fileinput.min.js',
		array( 'jquery', 'Popper', 'Boostrap-min' ),
		/*array( 'jquery', 'boostrap' ), */
		false,
		true
	);

	wp_enqueue_script(
		'bs_custom_file_input_script',
		'https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js',
		array( 'jquery', 'Popper', 'Boostrap-min', 'bootstrap_file_input_script' ),
		/*array( 'jquery', 'boostrap', 'bootstrap_file_input_script' ), */
		false,
		true
	);


	/* Scroll-triggered Counter Plugin */
	wp_enqueue_script(
		'purecounter_script',
		MST_THEME_URL . 'assets/js/purecounter.js',
		array( 'jquery' ),
		false,
		true
	);


	wp_enqueue_script(
		'common_script',
		MST_THEME_URL . 'assets/js/common.js',
		array( 'jquery', 'slick_script' ),
		filemtime( MST_THEME_DIR . 'assets/js/common.js' ),
		true
	);

	wp_localize_script(
		'common_script',
		'mst_common_vars',
		array(
			'theme_url'  => MST_THEME_URL,
			'assets_url' => MST_THEME_URL . 'assets'
		)
	);
}

add_action( 'wp_enqueue_scripts', 'mst_wp_enqueue_scripts' );
