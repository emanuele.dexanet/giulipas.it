<?php
/**
 * Recupera i post del Blog.
 *
 * @param  {string} $rule     : regola di caricamento dei post ("numberposts"|"monthsnum")
 * @param  {int}    $per_page : valore corrispondente alla regola (numero di post|numero di mesi)
 * @param  {int}    $page     : paginazione
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_get_blog_posts( $rule, $per_page, $page = 1 ) {
	if ( 'numberposts' == $rule ) {
		return mst_get_blog_posts_by_numberposts( $per_page, $page );
	} elseif ( 'monthsnum' == $rule ) {
		return mst_get_blog_posts_by_mounthsnum( $per_page, $page );
	} else {
		return array(
			'posts'         => array(),
			'show_loadmore' => false
		);
	}
}



/**
 * Recupera un "tot" numero di post.
 *
 * @param  {int} $posts_per_page : numero di post
 * @param  {int} $page           : paginazione
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_get_blog_posts_by_numberposts( $posts_per_page, $page ) {
	$query_args  = array(
		'posts_per_page'      => $posts_per_page,
		'orderby'             => 'date',
		'order'               => 'DESC',
		'post_type'           => 'post',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1
	);
	
	if ( $page > 1 ) {
		$query_args['paged'] = $page;
	}
	
	$loop = new WP_Query( $query_args );
	
	return array(
		'posts'         => $loop->posts,
		'show_loadmore' => $loop->found_posts >= $posts_per_page && $posts_per_page != -1
	);
}



/**
 * Recupera tutti i post di un "tot" numero di mesi.
 *
 * @param  {int} $monthsnum : numero di mesi
 * @param  {int} $page      : paginazione
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_get_blog_posts_by_mounthsnum( $monthsnum, $page ) {
	global $wpdb;
	
	$year_months  = $wpdb->get_results(
		"
		SELECT
			CONCAT(EXTRACT(YEAR FROM post_date), '-', EXTRACT(MONTH FROM post_date)) AS yearmonth
		FROM
			$wpdb->posts
		WHERE
			post_type = 'post'
			AND post_status = 'publish'
		GROUP BY
			yearmonth
		ORDER BY
			post_date DESC
		"
	);
	$date_range    = array_map( function( $n ) {
	   $array = explode( '-', $n->yearmonth );

	   return array(
			'year'  => $array[0],
			'month' => $array[1]    
		);
	}, $year_months );
	$start_index   = $monthsnum * ( $page -1 );
	$end_index     = ( $page * $monthsnum ) - 1;
	$show_loadmore = true;
	$query_args    = array(
		'posts_per_page'      => -1,
		'orderby'             => 'date',
		'order'               => 'DESC',
		'post_type'           => 'post',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'date_query'          => array()
	);
	$date_query    = array();
	
	if ( array_key_exists( $start_index, $date_range ) ) {
		$date                 = new DateTime( date( 'Y-m-t', strtotime( sprintf(
			'%1$s-%2$s-%3$s',
			$date_range[ $start_index ]['year'],
			$date_range[ $start_index ]['month'],
			1
		) ) ) );
		$before               = $date->format('Y-m-d');
		$date_query['before'] = $before;
	} else {
		$show_loadmore = false;
	}

	if ( array_key_exists( $end_index, $date_range ) ) {
		$after                   = sprintf(
			'%1$s-%2$s-%3$s',
			$date_range[ $end_index ]['year'],
			$date_range[ $end_index ]['month'],
			1
		);
		$date_query['after']     = $after;
		$date_query['inclusive'] = true;

	
		
		//if ( $end_index == array_key_last( $date_range ) ) {
		//	$show_loadmore = false;
		//}
	} else {
		$date_query['column'] = 'post_date';
		$show_loadmore        = false;
	}
	
	$query_args['date_query'][] = $date_query;
	$loop                       = new WP_Query( $query_args );
	
	return array(
		'posts'         => $loop->posts,
		'show_loadmore' => $show_loadmore
	);
}



/**
 * Raggruppa i post del Blog.
 *
 * @param  {array} $posts : 
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_group_posts( $posts ) {
	$groups = array();
	
	if ( !empty( $posts ) ) {
		foreach ( $posts as $post ) {
			$time                    = strtotime( $post->post_date );
			$year                    = date( 'Y', $time );
			$month                   = date( 'M', $time );
			$year_month              = "$year-$month";
			$groups[ $year_month ][] = new Timber\Post( $post->ID );
		}
	}
	
	return $groups;
}



/**
 * Recupera i custom post type "case-history".
 *
 * @param  {int}  $posts_per_page   : numero di post
 * @param  {int}  $taxonomy_term_id : ID del termine della tassonomia
 * @param  {int}  $page             : paginazione
 *
 * @return {array}
 *
 * @since 1.0.0
 */
function mst_get_case_history_posts( $posts_per_page, $taxonomy_term_id = 0, $page = 1 ) {
	$query_args = array(
		'posts_per_page' => $posts_per_page,
		'post_type'      => 'case-history',
		'order'          => 'DESC',
		'ordrby'         => 'date',
	);
	
	if ( $taxonomy_term_id > 0 ) {
		$query_args['tax_query'] = array(
			array(
				'taxonomy' => 'ch-category',
				'field'    => 'term_id',
				'terms'    => $taxonomy_term_id,
			)
		);
	}
	
	if ( $page > 1 ) {
		$query_args['paged'] = $page;
	}
	
	//$posts = new Timber\PostQuery( $query_args );
	$loop  = new WP_Query( $query_args );
	$posts = $loop->posts;
	$ch_posts = array();
	
	if ( !empty( $posts ) ) {
		foreach ( $posts as $post ) {
			$ch_posts[] = new Timber\Post( $post->ID );
		}
	}
	
	return array(
		'posts'         => $ch_posts,
		'show_loadmore' => count( $posts ) >= $posts_per_page && $posts_per_page != -1
	);
}