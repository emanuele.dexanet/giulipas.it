<?php
/**
 * Third party plugins that hijack the theme will call wp_head() to get the header template.
 * We use this to start our output buffer and render into the views/page-plugin.twig.php template in footer.php
 *
 * @since 1.0.0
 */

$GLOBALS['timberContext'] = Timber::context();
ob_start();
