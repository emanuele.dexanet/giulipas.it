(function($) {
	/**
	 * initializeBlock
	 *
	 * Adds custom JavaScript to the block HTML.
	 *
	 * @param  {object} $block     : the block jQuery element
	 * @param  {object} attributes : The block attributes (only available when editing)
	 *
	 * @return void
	 */
	var initializeBlock = function( $block, attributes ) {
		console.log(attributes);
		
		if ( isDefined(attributes) ) {
			var sectionSelector = $block.find('[data-type="mst-section"]');
			var acfBlockData    = attributes.data;
			
			if ( isDefined(acfBlockData) ) {
				Object.keys(acfBlockData).forEach(function(key) {
					if ( 'wpb_content_width' == key ) {
						var contentWidth = ( '' != acfBlockData[key] ) ? acfBlockData[key] : 'container-fluid';
						
						jQuery('[data-type="mst-section"] > div').addClass(contentWidth);
					}
				});
			}	
		}
	}

	
	
	// Initialize each block on page load (front end).
	$(document).ready(function() {
		/*$('[data-type="mst-section"]').each(function() {
			initializeBlock( $(this) );
		});*/
	});

	
	
	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=mst-section', initializeBlock );
	}

})(jQuery);