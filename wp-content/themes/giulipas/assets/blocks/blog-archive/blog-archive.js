var page = 1;

$(document).ready(function() {	
	/**
     * Gestisce il caricamenti di nuovi elementi.
     *
	 * @since 1.0.0
     */
	$('.load-more').on('click', function() {
		var that = $(this);
		
		that.blur();
		
		loadMorePosts(that);
	});
	
});



/**
 * Carica nuovi elementi.
 *
 * @param  {selector} loadMoreButton : 
 *
 * @return {void}
 *
 * @since 1.0.0
 */
function loadMorePosts(loadMoreButton) {
	displayLoading(loadMoreButton, true);
	page++;
	
	var blockId         = loadMoreButton.data('block-id');
	var defaultText     = loadMoreButton.data('default-text');
	var loadingText     = loadMoreButton.data('loading-text');
	var target          = loadMoreButton.data('target');
	var rule            = loadMoreButton.data('rule');
	var perPage         = loadMoreButton.data('perpage');
	var nonce           = loadMoreButton.data('nonce');
	var ancestorWrapper = jQuery('#loadmore-wrapper-' + blockId);
	
	jQuery.ajax({
		url: mst_blog_vars.ajax_url,
		type: 'POST',
		dataType: 'json',
		data: {
			action: 'load_more_posts',
			_nonce: nonce,
			page: page,
			block_id: blockId,
			rule: rule,
			per_page: perPage
		},
		success: function (response) {
			if ('data' in response && '' != response.data) {
				var dataKeys = jQuery.map(jQuery('.row.year-month', ancestorWrapper), function(element, index) {
					return jQuery(element).data('key');
				});
				
				jQuery.each( response.data, function(key, value) {
					if ( jQuery.inArray(key, dataKeys) > -1 ) {
						var parentWrapper = jQuery('.row.year-month[data-key="' + key + '"] .parent-wrapper');
						var yearMonthRow  = jQuery(value);
						var posts         = jQuery('.blog-items', yearMonthRow).html();
						
						parentWrapper.append(jQuery.trim(posts.replace(/\r?\n|\r/g, '')));
					} else {
						ancestorWrapper.append(jQuery.trim(value.replace(/\r?\n|\r/g, '')));
					}
				});
				
				displayLoading(loadMoreButton, false);
				
				if ( response.show_loadmore ) {
					jQuery('.load-more').attr('disabled', false).fadeIn();
				} else {
					jQuery('.load-more').attr('disabled', true).fadeOut();
				}
			} else {
				displayLoading(loadMoreButton, false);
				jQuery('.load-more').attr('disabled', true).fadeOut();
			}
		},
		error: function(xhrRequest, status, errorMessage) {
			displayLoading(loadMoreButton, false);
			console.log(xhrRequest);
		}
	});
}



/**
 * Mostra il loader.
 *
 * @param {selector} loadMoreButton
 * @param {bool}     show
 *
 * @since 1.0.0
 */
function displayLoading(loadMoreButton, show) {
    var button        = jQuery(loadMoreButton);
    var buttonWrapper = button.closest('.loadmore-button-wrapper');
	var target        = button.data('target');
    var loading       = jQuery('.ajax-loader', buttonWrapper);
	
	if ( show ) {
		//jQuery(target).addClass('loading');
		button.text(button.data('text-loading'));
        
        buttonWrapper.addClass('loading');
	} else {
		//jQuery(target).removeClass('loading');
		button.text(button.data('text-default'));
        
        buttonWrapper.removeClass('loading');
	}
	
	//button.attr('disabled', show);
}