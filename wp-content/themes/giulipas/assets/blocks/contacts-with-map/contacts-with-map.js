var map;

function initMap() {
	var giulipas = new google.maps.LatLng(45.63723479463178, 10.273919043192246);
	const pin    = {
		path: "m100 0c-55.1 0-100 45.5-100 101.4 0 69.4 89.5 171.3 93.3 175.6 3.6 4 9.8 4 13.4 0 3.8-4.3 93.3-106.2 93.3-175.6 0-55.9-44.9-101.4-100-101.4zm0 152.4c-27.7 0-50.3-22.9-50.3-51s22.6-51 50.3-51 50.3 22.9 50.3 51-22.6 51-50.3 51z",
		anchor: new google.maps.Point(100,280),
		fillColor: "#1342f0",
		fillOpacity: 1,
		scale: .15,
		strokeColor: "#ffffff",
		strokeWeight: 0
	};

	map = new google.maps.Map(document.getElementById("map-view"), {
		center: giulipas,
		mapId: '426d63537b4bbad1',
		zoom: 9,
		gestureHandling: "cooperative",
		zoomControl: true,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: false
	});

	const marker = new google.maps.Marker({
		position: giulipas,
		icon: pin,
		map: map
	});
}



$(document).ready(function() {
	
});