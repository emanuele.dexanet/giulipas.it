var page          = 1;
var filterChanged = false;
var filter        = 0;

$(document).ready(function() {	
	/**
     * Gestisce il caricamenti di nuovi elementi.
     *
	 * @since 1.0.0
     */
	$('.load-more').on('click', function() {
		var that = $(this);
		
		that.blur();
		
		loadMorePosts(that);
	});
	
	
	
	/**
     * Gestisce la selezione di un termine della tassonomia.
     *
	 * @since 1.0.0
     */
	$('.filters .list-item a').on('click', function() {
		var that             = $(this);
		var blockCtx         = that.closest('div[id^="block-"]');
		var loadMoreButton   = $('.load-more', blockCtx);
		var listItems        = $('.filters .list-items .list-item');
		var selectedListItem = that.parent();
		
		filterChanged = true;
		filter        = that.data('termid');
		
		listItems.removeClass('active');
		selectedListItem.addClass('active');
		
		loadMorePosts(loadMoreButton);
	});
	
	$('.filters select.filter-select').on('change', function() {
		var that           = $(this);
		var blockCtx       = that.closest('div[id^="block-"]');
		var loadMoreButton = $('.load-more', blockCtx);
		
		filterChanged      = true;
		filter             = that.val();
		
		loadMorePosts(loadMoreButton);
	});
	
});



/**
 * Carica nuovi elementi.
 *
 * @param  {selector} loadMoreButton : 
 *
 * @return {void}
 *
 * @since 1.0.0
 */
function loadMorePosts(loadMoreButton) {
	displayLoading(loadMoreButton, true);
	
	if ( !filterChanged ) {
		page++;
	} else {
		page = 1;
	}
	
	
	var blockId         = loadMoreButton.data('block-id');
	var defaultText     = loadMoreButton.data('default-text');
	var loadingText     = loadMoreButton.data('loading-text');
	var target          = loadMoreButton.data('target');
	var numberposts     = loadMoreButton.data('numberposts');
	var nonce           = loadMoreButton.data('nonce');
	var ancestorWrapper = jQuery('#loadmore-wrapper-' + blockId);
	var blockCtx        = ancestorWrapper.parent();
	
	jQuery.ajax({
		url: mst_ch_vars.ajax_url,
		type: 'POST',
		dataType: 'json',
		data: {
			action: 'load_more_ch_posts',
			_nonce: nonce,
			page: page,
			block_id: blockId,
			posts_per_page: numberposts,
			term_id: filter
		},
		success: function (response) {
			if ( filterChanged ) {
				ancestorWrapper.html('');

				filterChanged = false;
			}
			
			if ('data' in response && '' != response.data) {
				var items = jQuery.map(response.data, function(element, index) {
					return jQuery.trim(element.replace(/\r?\n|\r/g, ''));
				});
				
				jQuery.each(items, function(index, element) {
					ancestorWrapper.append(jQuery(element));
				});
				
				displayLoading(loadMoreButton, false);
				
				if ( response.show_loadmore ) {
					jQuery('.load-more').attr('disabled', false).fadeIn();
				} else {
					jQuery('.load-more').attr('disabled', true).fadeOut();
				}
			} else {
				ancestorWrapper.append('<div class="col-12"><p class="alert alert-info">' + response.message + '</p></div>');
				
				displayLoading(loadMoreButton, false);
				jQuery('.load-more').attr('disabled', true).fadeOut();
			}
		},
		error: function(xhrRequest, status, errorMessage) {
			displayLoading(loadMoreButton, false);
			console.log(xhrRequest);
		}
	});
}



/**
 * Mostra il loader.
 *
 * @param {selector} loadMoreButton
 * @param {bool}     show
 *
 * @since 1.0.0
 */
function displayLoading(loadMoreButton, show) {
	var button        = jQuery(loadMoreButton);
    var buttonWrapper = button.closest('.loadmore-button-wrapper');
	var target        = button.data('target');
    var loading       = jQuery('.ajax-loader', buttonWrapper);
	
	if ( show ) {
		//jQuery(target).addClass('loading');
		button.text(button.data('text-loading'));
        
        buttonWrapper.addClass('loading');
	} else {
		//jQuery(target).removeClass('loading');
		button.text(button.data('text-default'));
        
        buttonWrapper.removeClass('loading');
	}
	
	//button.attr('disabled', show);
}