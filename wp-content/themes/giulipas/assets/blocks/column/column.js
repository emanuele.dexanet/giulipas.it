(function($) {
	/**
	 * initializeBlock
	 *
	 * Adds custom JavaScript to the block HTML.
	 *
	 * @param  {object} $block     : the block jQuery element
	 * @param  {object} attributes : The block attributes (only available when editing)
	 *
	 * @return void
	 */
	var initializeBlock = function( $block, attributes ) {
		console.log('$block');
		console.log($block);
		console.log('attributes');
		console.log(attributes);
		
		if ( isDefined(attributes) ) {
			var blockCtx       = jQuery($block[0]);
			var columnSelector = jQuery('.acf-block-preview > [data-type="mst-column"]', blockCtx);
			var acfBlockData   = attributes.data;
			
			if ( isDefined(acfBlockData) ) {
				var gridLayout = [
					'wpb_grid_breakpoints_wpb_grid_small', 
					'wpb_grid_breakpoints_wpb_grid_medium', 
					'wpb_grid_breakpoints_wpb_grid_large', 
					'wpb_grid_breakpoints_wpb_grid_extra_large'
				];

				Object.keys(acfBlockData).forEach(function(key) {
					if ( $.inArray(key, gridLayout) > -1 && '' != acfBlockData[key] ) {
						columnSelector.addClass(acfBlockData[key]);
					}

				});
			}
		}
	}

	
	
	// Initialize each block on page load (front end).
	$(document).ready(function() {
		/*$('[data-type="mst-column"]').each(function() {
			initializeBlock( $(this) );
		});*/
	});

	
	
	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=mst-column', initializeBlock );
	}

})(jQuery);