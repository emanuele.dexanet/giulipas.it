AOS.init({
	once: true,
	startEvent: 'load',
});


(function($){
	$.fn.selectpicker.Constructor.BootstrapVersion = '4';
})(jQuery);


$(document).ready(function() {

	$('.hamburger').on('click', function() {
		$(this).toggleClass('is-active');
		$('#flyout-menu').toggleClass('in-view');
	});



    // Applicazione della classe "multiline" sui titoli delle hero section per gestire il corretto allineamento dei "cut" rispetto ai testi su 1 o 2 righe.
	if ($('.cut-before').height() > parseInt($('.cut-before').css('line-height'))) {
		$('.title').addClass('multiline');
	} else {
		$('.title').removeClass('multiline');
	}



    // Attivazione della modalità "fixed" dell'header in presenza di una hero section.
	// Contestualmente viene anche spostato il breadcrumb
	if ( $('main section').first().hasClass('hero-section') ) {
		$('#site-header').addClass('negative');
		$('#content').removeClass('fixed-nav');

		$('#breadcrumb').insertAfter($('.hero-section'));

		var waypoints = $('#site-header').waypoint({
			handler: function(direction) {
				var header = $('#site-header');

				if ( (header.offset().top <= header.height()) ) {
					if ( direction == 'down' ) {
						$('#site-header').removeClass('negative');
						$('#content').addClass('fixed-nav');
					} else {
						$('#site-header').addClass('negative');
						$('#content').removeClass('fixed-nav');
					}
				}
			},
			offset: function() {
				return window.pageYOffset - $('#site-header').height();
			}
		});
	}



	$('.scroll-down').on('click', function() {
		var scrollTo = $($('section')[1]).offset().top - $('header').height();

		$('html, body').animate({
			scrollTop: scrollTo
		}, 200, function() {

		});
	});


	if ( $('.fade-item').length > 0 ) {
		

		setTimeout(function() {
			$('.fade-item').css('opacity', '1');
		 }, 200);
	}



	// Slideshow "Solo immagini o video".
	if ( $('section.slideshow.pictures').length > 0 ) {
		$('section.slideshow.pictures').each(function(i, el) {
			var ctx = $(this);

			$('.slick-slides', ctx).slick({
				adaptiveHeight: true,
				autoplay: true,
				autoplaySpeed: 3000,
				arrows: true,
				appendArrows: $('.slick-arrows', ctx),
				prevArrow: '<img src="' + mst_common_vars.assets_url + '/images/prev.svg" class="slick-prev">',
				nextArrow: '<img src="' + mst_common_vars.assets_url + '/images/next.svg" class="slick-next">',
				cssEase: 'ease',
				dots: false,
				draggable: true,
				fade: true,
				focusOnSelect: false,
				easing: 'linear',
				infinite: true,
				lazyLoad: 'ondemand',
				mobileFirst: false,
				pauseOnFocus: true,
				pauseOnHover: true,
				pauseOnDotsHover: false,
				respondTo: 'window',
				responsive: 'none',
				speed: 300,
				swipe: true,
				swipeToSlide: false,
				touchMove: true,
				touchThreshold: 5,
				useCSS: true,
				useTransform: true,
				variableWidth: false,
				waitForAnimate: true,
				zIndex: 1000,
			});
			// .on('setPosition', function (event, slick) {
			// 	slick.$slides.css('height', slick.$slideTrack.height() + 'px');
			// });
		});
	}


	// Slideshow "Immagini e testo con menu di navigazione".
	if ( $('section.slideshow.with-navigation').length > 0 ) {
		var breakpoint = window.matchMedia("(max-width: 575px)").matches ? 'xs' :
						 window.matchMedia("(max-width: 767px)").matches ? 'sm' :
						 window.matchMedia("(max-width: 959px)").matches ? 'md' :
						 window.matchMedia("(max-width: 1139px)").matches ? 'lg' :
						 window.matchMedia("(min-width: 1140px)").matches ? 'xl' :
						 '';
		var padding    = ( breakpoint === 'md' || breakpoint === 'lg' || breakpoint === 'xl' ) ? 100 : 60;

		$('section.slideshow.with-navigation').each(function(i, el) {
			var ctx = $(this);

			$('.slick-slides', ctx).slick({
				adaptiveHeight: false,
				autoplay: true,
				autoplaySpeed: 3000,
				arrows: false,
				cssEase: 'ease',
				dots: true,
				appendDots: $('.slick-nav', ctx),
				customPaging: function(slick, index) {
					var title = $(slick.$slides[index]).data('title');

					return '<a class="nav-item">' + title + '</a>';
				},
				dotsClass: 'slick-dots',
				draggable: true,
				fade: true,
				focusOnSelect: false,
				easing: 'linear',
				infinite: true,
				lazyLoad: 'ondemand',
				mobileFirst: false,
				pauseOnFocus: true,
				pauseOnHover: true,
				pauseOnDotsHover: false,
				respondTo: 'window',
				responsive: 'none',
				speed: 300,
				swipe: true,
				swipeToSlide: false,
				touchMove: true,
				touchThreshold: 5,
				useCSS: true,
				useTransform: true,
				variableWidth: false,
				waitForAnimate: true,
				zIndex: 1000,
			}).on('setPosition', function (event, slick) {
				slick.$slides.css('height', slick.$slideTrack.height() + 'px');
			});

			if ( 'md' == breakpoint || 'lg' == breakpoint || 'xl' == breakpoint ) {
				var headerHeight = $('.slideshow-header-container', ctx).height();

				$('.slick-slides .slide', ctx).css('height', (headerHeight + 2 * padding) + 'px');
				ctx.css('height', (headerHeight + 2 * padding) + 'px');
			} else {
				$('.slick-slides .slide', ctx).css('height', (parseInt($('.slick-slides .slide', ctx).css('height')) + 2 * padding) + 'px');
				//ctx.css('height', (headerHeight + 2 * padding) + 'px');
			}
		});
	}


	// Slideshow "Più elementi di testo".
	if ( $('section.slideshow.filmstrip').length > 0 ) {
		$('section.slideshow.filmstrip').each(function(i, el) {
			var ctx = $(this);

			$('.slick-slides', ctx).slick({
				adaptiveHeight: false,
				autoplay: true,
				autoplaySpeed: 3000,
				arrows: false,
				cssEase: 'ease',
				dots: false,
				draggable: true,
				fade: false,
				focusOnSelect: false,
				easing: 'linear',
				infinite: false,
				lazyLoad: 'ondemand',
				mobileFirst: true,
				pauseOnFocus: false,
				pauseOnHover: true,
				pauseOnDotsHover: false,
				respondTo: 'slider',
				//slide: '',
				slidesPerRow: 1,
				slidesToShow: 1,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 1140,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
						}
					},
					{
						breakpoint: 960,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 576,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				],
				speed: 300,
				swipe: true,
				swipeToSlide: false,
				touchMove: true,
				touchThreshold: 5,
				useCSS: true,
				useTransform: true,
				variableWidth: false,
				vertical: false,
				waitForAnimate: false,
				zIndex: 1000,
			});

			setBootstrapLayout();

			$('.slick-slides', ctx).on('breakpoint', function(event, slick, breakpoint) {
				setBootstrapLayout();
			});
		});
	}


	// Slideshow "Più elementi con immagini".
	if ( $('section.slideshow.filmstrip-pictures').length > 0 ) {
		$('section.slideshow.filmstrip-pictures').each(function(i, el) {
			var ctx = $(this);

			$('.slick-slides', ctx).slick({
				adaptiveHeight: false,
				autoplay: true,
				autoplaySpeed: 3000,
				arrows: false,
				cssEase: 'ease',
				dots: false,
				draggable: true,
				fade: false,
				focusOnSelect: false,
				easing: 'linear',
				infinite: false,
				lazyLoad: 'ondemand',
				mobileFirst: true,
				pauseOnFocus: false,
				pauseOnHover: true,
				pauseOnDotsHover: false,
				respondTo: 'slider',
				//slide: '',
				slidesPerRow: 1,
				slidesToShow: 1,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 1140,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
						}
					},
					{
						breakpoint: 960,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 576,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				],
				speed: 300,
				swipe: true,
				swipeToSlide: false,
				touchMove: true,
				touchThreshold: 5,
				useCSS: true,
				useTransform: true,
				variableWidth: false,
				vertical: false,
				waitForAnimate: false,
				zIndex: 1000,
			}).on('setPosition', function (event, slick) {
				slick.$slides.css('height', slick.$slideTrack.height() + 'px');
			});
		});
	}


	// Slideshow "Hero".
	if ( $('section.slideshow.hero').length > 0 ) {
		$('section.slideshow.hero').each(function(i, el) {
			var ctx = $(this);

			$('.slick-slides', ctx).slick({
				adaptiveHeight: false,
				autoplay: true,
				autoplaySpeed: 5000,
				arrows: false,
				cssEase: 'ease',
				dots: true,
				appendDots: $('.slick-nav', ctx),
				customPaging: function(slick, index) {
					var title = $(slick.$slides[index]).data('title');

					return '<a class="nav-item">' + title + '</a>';
				},
				dotsClass: 'slick-dots',
				draggable: true,
				fade: true,
				focusOnSelect: false,
				easing: 'linear',
				infinite: true,
				lazyLoad: 'ondemand',
				mobileFirst: false,
				pauseOnFocus: true,
				pauseOnHover: true,
				pauseOnDotsHover: false,
				respondTo: 'window',
				responsive: 'none',
				speed: 300,
				swipe: true,
				swipeToSlide: false,
				touchMove: true,
				touchThreshold: 5,
				useCSS: true,
				useTransform: true,
				variableWidth: false,
				waitForAnimate: true,
				zIndex: 1000,
			});
			// .on('setPosition', function (event, slick) {
			// 	slick.$slides.css('height', slick.$slideTrack.height() + 'px');
			// });
		});
    }



	if ($('input[type="file"]').length > 0) {
		//bsCustomFileInput.init();
		$('input[type="file"]').on('change', function(){
				var ctx = $(this).closest('.custom-file');
				$('label', ctx).text(this.files[0].name);
		});
	}


	if ( $('#help').length > 0 ) {
		$('#help').selectpicker({
			style: 'btn',
			size: 'auto',
			width: '100%',
			container: 'body',
            mobile: true
		});
	}

	if ( $('.filter-select').length > 0 ) {
		$('.filter-select').selectpicker({
			style: 'btn',
			size: 'auto',
			width: '100%',
			container: 'body',
            mobile: true
		});
	}



	// Gestisce l'apertura dell'elenco dei social network per la condivisione.
    $('.social-sharing a.share-toggle').on('click', function() {
        var that = $(this);
        var ctx = that.closest('.social-sharing');

        ctx.toggleClass('open');
    });



	// Gestisce i video all'interno di una modal.
	$('.videoModal').on('show.bs.modal', function (e) {
		var iframe = $('iframe', $(this));

		iframe.attr('src', iframe.data('src'));
	});

	$('.videoModal').on('hide.bs.modal', function (e) {
		var iframe = $('iframe', $(this));

		iframe.attr('src', '');
	});



    // Gestisce l'apertura della modal popup con il video.
    $('.play-button[data-toggle="modal"]').on('click', function(e) {
        e.preventDefault();

        var that     = $(this);
        var videoUrl = that.data('src');

        $('#modal-video iframe').attr('src', videoUrl);
        $('#modal-video').modal('show');
    });

    $('#modal-video').on('hidden.bs.modal', function(e) {
        $('#modal-video iframe').attr('src', '');
    })

});





/*
 * Restituisce true se "element" esiste, altrimenti false.
 *
 * @param {*} element : può essere un qualsiasi elemento del DOM o un cookie
 */
function isDefined(element) {
	return ( typeof element !== typeof undefined );
}



/*
 * Restituisce true se "fn" è una funzione.
 *
 * @param {string} fn : nome di funzione
 */
function isFunction(fn) {
	return typeof fn === 'function';
}



function setBootstrapLayout() {
	$('section.slideshow.filmstrip').each(function(index, element) {
		var ctx        = $(element);
		var rowWidth   = $('.slick-slides', ctx).width();
		var slickTrack = $('.slick-track', ctx).width();
		var marginLeft = 8.333333 * rowWidth / slickTrack;

		$('.slide', ctx).css('margin-left', marginLeft + '%');
	});
}
