jQuery(document).ready(function() {
	if ( typeof acf == 'undefined' ) { 
		return;
	}
	
	var sliders             = JSON.parse(mst_ajax_vars.sliders);
	var blockCtx            = '.wp-block[data-type="acf/mst-slideshow"]';
	var filterSlidersSelect = '[data-key="field_607d8e0fe7ed2"] select';
	var filterSlidesSelect  = '[data-key="field_607d8e5742ca3"] select';
	var instanceAS          = new acf.Model({
		events: {
			'change [data-key="field_607d8e0fe7ed2"] select': 'onChangeFilterSlider',
		},
		onChangeFilterSlider: function(e, $el) {
			var currentBlock = $el.closest('.acf-fields');
			var sliderId     = $el.val();
			
			// Svuoto la select relativa alle slide.
			jQuery('option[value!=""]', jQuery(filterSlidesSelect, currentBlock)).remove();
			
			if ( '' != sliderId ) {
				var slides = sliders[sliderId];
				
				// Popolo la select relativa alle slide.
				fillFilterSlidesSelect(sliderId, jQuery(filterSlidesSelect, currentBlock), slides);
			}
		}
	});
	
	// Elimino da tutte le select delle slide quelle non corrispondenti allo slider selezionato.
	clearFilterSlidesSelect(filterSlidersSelect, filterSlidesSelect, blockCtx, sliders);
	
});



/**
 * Popola la select relativa alle slide.
 *
 * @param {int}    sliderId : ID dello slider
 * @param {object} select   : selettore relativo alla select delle slide
 * @param {array}  options  : array con le opzioni
 *
 */
function fillFilterSlidesSelect(sliderId, select, options) {
	jQuery.each(options, function(index, element) {
		select.append('<option value="' + element.value + '">' + element.label + '</option>');
	});
}



/**
 * Elimina le slide non pertinenti dalla relativa select.
 *
 * @param {string} sliderSelect : selettore relativo alla select dello slider
 * @param {string} slidesSelect : selettore relativo alla select delle slide
 * @param {string} context      : contesto
 * @patam {object} sliders      : 
 *
 */
function clearFilterSlidesSelect(sliderSelect, slidesSelect, context, sliders) {
	jQuery.each(jQuery(sliderSelect, context), function(fIndex, fElement) {
		var currentBlock   = jQuery(fElement).closest('.acf-fields');
		var selectedSlider = jQuery(fElement).val();
		
		if ( '' != selectedSlider ) {
			var sliderId = selectedSlider;
			
			jQuery.each(jQuery('option[value!=""]', jQuery(slidesSelect, currentBlock)), function(cIndex, cElement) {
				var slideOption = cElement;
				var slideId     = parseInt(slideOption.value);
				var slideIds    = jQuery.map(sliders[sliderId], function(e, i) {
					return e.value;
				});

				if ( slideIds.indexOf(slideId) == -1 ) {
					jQuery(slideOption).remove();
				}
			});
		} else {
			// Svuoto la select relativa alle slide.
			jQuery('option[value!=""]', jQuery(slidesSelect, currentBlock)).remove();
		}
	});
}

