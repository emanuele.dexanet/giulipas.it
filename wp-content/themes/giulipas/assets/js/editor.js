wp.domReady( function() {
	// Register custom Button styles.
	wp.blocks.registerBlockStyle(
		'core/button',
		[
			{
				name: 'ghost-default',
				label: 'Ghost default',
				isDefault: true
			},
			{
				name: 'ghost-primary',
				label: 'Ghost primary',
			},
			{
				name: 'filled-default',
				label: 'Filled default',
			},
			{
				name: 'filled-primary',
				label: 'Filled primary',
			},
			/*{
				name: 'cta-arrow',
				label: 'Arrow Link',
			},
			{
				name: 'cta-text-arrow',
				label: 'Text Arrow Link',
			}*/
		]
	);
	
	// Unregister Button styles.
	wp.blocks.unregisterBlockStyle(
		'core/button',
		[ 'default', 'outline', 'squared', 'fill' ]
	);
	
	
	
	(function () {
		const { SVG, Path } = wp.components;
		const giulipasIcon  = React.createElement(
			SVG, 
			{
				width: "24",
				height: "24",
				viewBox: "0 0 74.7 49.9",
				xmlns: "http://www.w3.org/2000/svg"
			}, 
			React.createElement(
				Path, 
				{
					d: "m24.9 0c-13.7 0-24.9 11.2-24.9 24.9s11.2 24.9 24.9 24.9h31.5v-31.5h-30.9c-3.6 0-6.6 2.9-6.7 6.5 0 3.6 2.9 6.6 6.5 6.7h.2 16c1.2-.1 2.3.9 2.3 2.1.1 1.2-.9 2.3-2.1 2.3-.1 0-.2 0-.3 0h-16c-6.1 0-11-4.9-11-11s4.9-11 11-11h35.3v36h13.8v-49.9z"
				}
			)
		);
		
		wp.blocks.updateCategory('giulipas', { icon: giulipasIcon } );
	})();
} );





/*
 * Restituisce true se "element" esiste, altrimenti false.
 *
 * @param {*} element : può essere un qualsiasi elemento del DOM o un cookie
 */
function isDefined(element) {
	return ( typeof element !== typeof undefined );
}



/*
 * Restituisce true se "fn" è una funzione.
 *
 * @param {string} fn : nome di funzione
 */
function isFunction(fn) {
	return typeof fn === 'function';
}