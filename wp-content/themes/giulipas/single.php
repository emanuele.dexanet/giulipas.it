<?php
/**
 * The Template for displaying all single posts
 *
 * @since 1.0.0
 */

$context         = Timber::context();
$timber_post     = Timber::get_post();
$context['post'] = $timber_post;
$reusable_blocks = get_field( 'wppt_reusable_blocks', $timber_post->ID );
$extra_blocks    = array();

if ( $reusable_blocks ) {
	foreach ( $reusable_blocks as $block_id ) {
		$block          = get_post( $block_id );
		$extra_blocks[] = apply_filters( 'the_content', $block->post_content );
	}
}

$context['extra_content'] = implode( '', $extra_blocks );

if ( post_password_required( $timber_post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $timber_post->ID . '.twig', 'single-' . $timber_post->post_type . '.twig', 'single-' . $timber_post->slug . '.twig', 'single.twig' ), $context );
}
