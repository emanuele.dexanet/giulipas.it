<?php
/**
 * Third party plugins that hijack the theme will call wp_head() to get the header template.
 * We use this to start our output buffer and render into the views/page-plugin.twig.php template in footer.php
 *
 * @since 1.0.0
 */

$timberContext = $GLOBALS['timberContext'];

if ( !isset( $timberContext ) ) {
	throw new \Exception( 'Timber context not set in footer.' );
}

$timberContext['content'] = ob_get_contents();
ob_end_clean();

Timber::render( 'page-plugin.twig', $timberContext );
